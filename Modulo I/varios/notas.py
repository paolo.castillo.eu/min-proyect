from itertools import chain

class Menu:
    
    def printMenu():
        print("Bienvenido al menu principal de nota\n"+
              "Debe elejir una de la siguientes opciones\n"+
              "1) Guadar nota\n"+
              "2) Eliminar nota\n"+
              "3) Actualizar nota\n"+
              "4) Consultar nota\n"+
              "0) Para salir del programa")

class Nota:

    listNota = []    
    
    def save(nota):
        if Nota.is_numeric(nota) == True:
            Nota.listNota.append( [ (len(Nota.listNota)+1) ,nota ] )
    
    def delete(position):
        if Nota.checkElement(Nota.listNota) == True:
            Nota.listNota.pop(position)
    
    def update(position, nota):
        if Nota.is_numeric(nota) == True and Nota.checkElement(Nota.listNota) == True:
            Nota.listNota[position] = [position, nota]
    
    def checkElement(index):
        return index in chain.from_iterable(Nota.listaNota)
    
    def get():
        for i in Nota.listNota:
            print(f"POSICIÓN:{i[2].ljust(16)} NOTA: {str(i[1]).ljust(16)}")
    
    def is_numeric(nota):
        try:
            val = int(nota)
            return True
        except:
            print("Debe ingresar un numero")
        
        return False