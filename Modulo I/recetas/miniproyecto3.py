# -*- coding: utf-8 -*-
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %      
% PROYECTO 3: RECETAS                                                   %
%                                                                       %
% --------------------------------------------------------------------- %
%                                                                       %
% Created on Jul 27 22:47:00 2021                                       %
% @author: Paolo Castillo                                               %
% https://gitlab.com/paolo.castillo.eu/min-proyect                      %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

import os
import time
import unicodedata

def getPath(file):
    return os.path.join(
        os.path.dirname(__file__), file)

def removeAccents(text):
    text = str(text)
    normalized = unicodedata.normalize("NFKD", text)
    normalized = "".join(
        [c for c in normalized if not unicodedata.combining(c)])
    return normalized

def check(recipe, item):
    return True if item in recipe else False

def clearScreen():
    os.system('cls')

def error():
    print("Error")

class Ingredient:

    def setIngredient(self):
        file = open(getPath("ingredientes.txt"), "r", encoding="utf-8")
        listRecipe = Ingredient.load(file.readlines())
        file.close()
        return listRecipe
    
    def load(file):
        items = []
        for row in file:
            row = removeAccents(row.strip().upper()).split(' ')
            items.append(row)
        return items
    
    def reWriteFile(self, list):
        file = open(getPath("ingredientes.txt"), "w", encoding="utf-8")
        for row in list:
            file.writelines(f"{row[0]} {row[1]}\n")
        file.close()
    
    def get(lists):
        print("STOCK ACTUAL DE INGREDIENTES DISPONIBLES")
        for list in lists:
            print(f"{list[0]} {list[1]}")

    def add(ingredient, items):
        if len(items) > 0:
            for item in items:
                index_row = Ingredient.getIndexRow(ingredient, item)
                if len(index_row):
                    ingredient[index_row[0]][1] = int(
                        ingredient[index_row[0]][1]) + 1
                else:
                    ingredient.append([item, 1])
                    print(f"Nueva materia prima {item}")
        else:
            print("*** Debe ingresar el ingrediente a reponer ***")
    
    def set(recipe, ingredient, items):
        checkStock = 0
        stockMissing = []
        if len(items) > 0:
            if check(recipe, items[0]):
                recipeIngredients =  recipe[items[0]]
                
                for recipeIngredient in recipeIngredients:
                    index_row = Ingredient.getIndexRow(
                        ingredient, recipeIngredient)
                    if len(index_row) and int(ingredient[index_row[0]][1]) > 0:
                        checkStock+=1
                    else:
                        if len(index_row):
                            stockMissing.append(ingredient[index_row[0]][0])
                        else:
                            stockMissing.append(recipeIngredient)
                
                if checkStock == len(recipeIngredients):
                    for recipeIngredient in recipeIngredients:
                        index_row = Ingredient.getIndexRow(
                            ingredient, recipeIngredient)
                        ingredient[index_row[0]][1] = int(
                            ingredient[index_row[0]][1]) - 1
                else:
                    print(
                        f"*** No se puede hacer {items[0]} porque falta {','.join(stockMissing)} ***")
            
            else:
                print(f"*** Lo sentimos pero no preparamos {items[0]} ***")
        else:
            print("*** Debe ingresar el plato a preparar ***")

    def getIndexRow(list, text):
        return [list.index(row)
                for row in list if text in row]

class Recipe:
        
    def setRecipe(self):
        file = open(getPath("recetas.csv"), "r", encoding="utf-8")
        listRecipe = Recipe.load(file.readlines())
        file.close()
        return listRecipe
    
    def load(file):
        items = {}
        for row in file:
            row = removeAccents(row.strip().upper()).split(',')
            items[row[0]] = row[1:]
        return items

    def reWriteFile(self, list):
        file = open(getPath("recetas.csv"), "w", encoding="utf-8")
        for row in list.items():
            ingredient = ''
            if (len(row) > 1):
                ingredient = ','.join(row[1])
            file.writelines(f"{row[0]},{ingredient}\n")
        file.close()

class Menu:

    playSystem = True
    recipe = []
    ingredient = []

    options = [
        "PREPARAR",
        "REPONER",
        "STOP"
    ]

    def printWelcome(self):
        Menu.loadMenu()
        print("Bienvenido al restaurant los dorados\n"+
              "El sistema contiene las siguientes intrucción:\n"+
              "1) PREPARAR viene seguida del nombre de una de las recetas.\n"+
              "2) REPONER viene seguida de una lista de ingredientes separados por espacios.\n"+
              "3) STOP para detener el sistema")

    def printError():
        print("Por favor ingrese una opción correcta\n"+
              "para operar el sistema.")

    def loadMenu():
        recipe = Recipe()
        Menu.recipe = recipe.setRecipe()
        ingredient = Ingredient()
        Menu.ingredient = ingredient.setIngredient()

    def startSystem(self):
        joined_string = ",".join(Menu.options)
        option = input(f"Ingresa la opción {joined_string}:").upper()
        space = option.split(' ')
        if (len(space) > 0 and Menu.checkOptionMenu(space[0]) == True):
            time.sleep(2)
            # Cut string input
            option = space[0]
            space.pop(0)
            # Option menu
            if option == "PREPARAR":
                Ingredient.set(Menu.recipe, Menu.ingredient, space)
            elif option == "REPONER":
                Ingredient.add(Menu.ingredient, space)
            else:
                Menu.close()
            Ingredient.get(Menu.ingredient)
            
            # Profe como es posible manejar el menu por lista
            # Al intentar con la lista me ejecuta todas las opciones
            # antes de enviar el parametro[0] que es la opción del menu
            # como se indica en la linea de abajo 
            # |
            # switch_menu = {
            #     "PREPARAR": Ingredient.set(Menu.recipe, Menu.ingredient, space),
            #     "REPONER": Ingredient.add(Menu.ingredient, space),
            #     "STOP": lambda: Menu.close()
            # }
            
            Menu.reWrite()
        else:
            Menu.printError()
            
    def checkOptionMenu(option):
        x = Menu.options.count(option)
        return True if x > 0 else False

    def reWrite():
        ingredient = Ingredient()
        ingredient.reWriteFile(Menu.ingredient)
        recipe = Recipe()
        recipe.reWriteFile(Menu.recipe)

    def close():
        Menu.playSystem = False
        time.sleep(2)

clearScreen()
menu = Menu()
menu.printWelcome()

while menu.playSystem:
    menu.startSystem()
