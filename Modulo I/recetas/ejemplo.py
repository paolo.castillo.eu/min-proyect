import os

def obtenerDirectorio(file):
    return os.path.join(os.path.dirname(__file__), file)

def cargaIngrediente(file):
    items = []
    for row in file:
        row = row.strip().upper().split(' ')
        # ["TOMATE", "3"]
        items.append(row)
    return items

def cargarReceta(file):
    items = {}
    for row in file:
        row = row.strip().upper().split(',')
        items[row[0]] = row[1:]
    return items


def prepararPlato(plato, recetas, ingredientes):
    ingrediente = recetas[plato]
    sinStock = 0
    for row in ingrediente:
        index_row = getIndexRow(
            ingredientes, row)
        if (int(ingredientes[index_row[0]][1]) > 0):
            sinStock = sinStock + 1
        # print(ingredientes[index_row[0]])
    
    if (sinStock == len(ingrediente)):
        for row in ingrediente:
            index_row = getIndexRow(
                ingredientes, row)
            # print(sinStock)
            ingredientes[index_row[0]][1] = int(ingredientes[index_row[0]][1]) - 1
            
    # ingrediente = recetas["PASTELDECARNE"]
    # print(ingredientes)
    # print(ingrediente)

def getIndexRow(list, text):
    return [list.index(row)
            for row in list if text in row]

A = os.path.join(os.path.dirname(__file__), '..')
# A is the parent directory of the directory where program resides.

B = os.path.dirname(os.path.realpath(__file__))
# B is the canonicalised (?) directory where the program resides.

C = os.path.abspath(os.path.dirname(__file__))
# C is the absolute path of the directory where the program resides.

print(obtenerDirectorio("recetas.csv"))
print(obtenerDirectorio("ingredientes.txt"))

ing = open(obtenerDirectorio("ingredientes.txt"), "r", encoding="utf-8")
ingredientes = cargaIngrediente(ing)

rep = open(obtenerDirectorio("recetas.csv"), "r", encoding="utf-8")
recetas = cargarReceta(rep)

prepararPlato("PASTELDECARNE", recetas, ingredientes)
print(ingredientes)

ing = open(obtenerDirectorio("ingredientes.txt"), "w", encoding="utf-8")
for line in ingredientes:
    ing.writelines(f" {line[0]} {line[1]}\n")
ing.close()

# print(ingredient)
# print(ingredient[0][0])
# 'PastelDeCarne': ['Carne', 'Papa', 'Cebolla'],
# 'EnsaladaEspecial': ['Lechuga', 'Espárragos', 'Tomate']
# print(ingredient)
# print(recetas)
