# -*- coding: utf-8 -*-
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %      
% PROYECTO 1: ADIVINAR NUMERO                                           %
%                                                                       %
% --------------------------------------------------------------------- %
%                                                                       %
% Created on May 27 19:37:41 2021                                       %
% @author: Paolo Castillo                                               %
% https://gitlab.com/paolo.castillo.eu/min-proyect                      %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""
import random
import time

def start():
    print('Hola ! \n Bienvenido adivinador de numeros \n ¿ Como te llamas jugardor ?')
    username = input().capitalize()
    print('{} Piensa un numero entre el 1 y 100'.format(username))
    getSettings(1, 100, username)
    
def getSettings(minNumber, maxNumber, username):
    answer = random.randint(minNumber,maxNumber)
    # answer = 85
    itents = 0
    while True:
        itents+=1
        guess=input('Ingrese el numero a adivinar (0 para parar) :\n')
        try:
            guess = abs(int(guess))
            
            if (guess > 100):
                getQuestion('menor', itents, username, maxNumber)
                break
            elif (guess == 0):
                getQuestion('salir', itents, username, maxNumber)
                break
            elif (answer == guess):
                getQuestion('ok', itents, username, maxNumber)
                time.sleep(3)
                break
            elif (answer - guess) in range(20, 101):
                getQuestion('dis_20', itents, username, maxNumber)
            elif (answer - guess) in range(10, 20):
                getQuestion('dis_10', itents, username, maxNumber)
            elif  (answer - guess) in range(5,10):
                getQuestion('dis_5', itents, username, maxNumber)
            elif guess < 5:
                getQuestion('dis_me', itents, username, maxNumber)
            elif answer < guess:
                getQuestion('mayor', itents, username, maxNumber)
            else:
                getQuestion('cerca', itents, username, maxNumber)
                
        except ValueError:
            print('Por favor ingrese un valor numerico')

def getQuestion(range, itents, username, maxNumber):
    message = {
        'ok' : "Felicitaciones {}, lo lograste en {} intentos".format(username, itents),
        'menor' : "Sorry {}, pero debes ingresar un numero menor a {}".format(username, maxNumber),
        'salir': "No lo lograste {} a pesar de tratar {} veces. Mas suerte para otra vez".format(username, itents),
        'dis_5': "Sorry {}, ese no es pero estas a una distancia mayor que 5 y menor que 10".format(username),
        'dis_10': "Sorry {}, ese no es pero estas a una distancia mayor que 10 y menor que 20".format(username),
        'dis_20' : "Sorry {}, ese no es pero estas a una distancia de 20".format(username),
        'dis_me' : "Sorry {}, ese no es pero estas a una distancia menor a 5 ".format(username),
        'mayor' : "Sorry {}, ese no es pero estas a una distancia mayor ".format(username),
        'cerca' : "Sorry {}, ese no es pero cerca ".format(username)
    }
    print(message[range])

if __name__ == "__main__":
    start()
