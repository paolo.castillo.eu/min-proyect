import random

minNumber = 1
maxNumber = 100

print('Hola ! \n bienvenido adivinador de numeros \n ¿ Como te llamas jugardor ?')
username = input().capitalize()
print('{} Piensa un numero entre el 1 y 100'.format(username))

answer = random.randint(minNumber,maxNumber)
# answer = 85

itents = 0

while True:
    guess=input('Ingrese el numero a adivinar (0 para parar) :\n')
    itents+=1
    try:
        guess = abs(int(guess))
        
        # print (answer - guess)
        if (guess > 100):
            print("Sorry {}, pero debes ingresar un numero menor a {}".format(username, maxNumber))
            break
        elif (guess == 0):
            print("No lo lograste {} a pesar de tratar {} veces. Mas suerte para otra vez".format(username,itents))
            break
        elif (answer == guess):
            print("Felicitaciones {}, lo lograste en {} intentos".format(username, itents))
            break
        # elif (answer - guess) >= 20:
        elif (answer - guess) in range(20, 100):
            print("Sorry {}, ese no es pero estas a una distancia mayor que 20".format(username))
        # elif (answer - guess) >= 10 and (answer - guess) < 20:
        elif (answer - guess) in range(10, 20):
            print("Sorry {}, ese no es pero estas a una distancia mayor que 10 y menor que 20".format(username))
        # elif (answer - guess) >= 5 and (answer - guess) < 10 or 5 > :
        elif  (answer - guess) in range(5,10):
            print("Sorry {}, ese no es pero estas a una distancia mayor que 5 y menor que 10".format(username))
        elif guess < 5:
            print("Sorry {},  ese no es pero estas a una distancia menor a 5 ".format(username))
        
    except ValueError:
        print('Por favor ingrese un valor numerico')
        