# -*- coding: utf-8 -*-
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %      
% PROYECTO 2: DARDOS                                                    %
%                                                                       %
% --------------------------------------------------------------------- %
%                                                                       %
% Created on Jun 27 19:37:41 2021                                       %
% @author: Paolo Castillo                                               %
% https://gitlab.com/paolo.castillo.eu/min-proyect                      %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

import os
import time
from random import randint
from itertools import chain

class Game:

    playGame = True
    rounds = 3
    
    pointList = [
        ["DOUBLE BULL 50 POINT", 50, 1],
        ["SINGLE BULL 25 POINT", 25, 1],
        ["TRIPLE RING 3X", 0, 3],
        ["DOUBLE RING 2X", 0, 2],
        ["SINGLE RING 1X", 0, 1],
        ["SINGLE RING 1X", 0, 1]
    ]

    @staticmethod
    def printWelcome():
        print("Bienvenido al juego de dardos!" +
            "\nEl juego tiene las siguientes reglas: "+
            "\n\n 1) Los jugadores siempre empiezan con 501 puntos. "+
            "\n 2) Cada punto se restan a los puntos iniciales."+
            "\n 3) Si un jugardor llega a cero gana el juego."+
            "\n 4) Si un jugador tiene 30 punto y sus ultimos"+
            "\n dardos suman 45, al restar los 45 de los 30 quedaran -15"+
            "\n que cambian a 15")

    @staticmethod        
    def clearScreen():
        os.system('cls')
    
    @staticmethod
    def startGame():
        Game.clearScreen()
        Game.printScore()
        for player in PlayerHelper.playerList:
            if player[1] == 0:
                print(f"Gana {player[0]}! Felicitaciones")
                Game.playGame = False
                time.sleep(3)
                
            if Game.playGame == True:
                Game.roundGame(player)
                time.sleep(2)
            
    
    @staticmethod
    def roundGame(player):
        for r in range(Game.rounds):
            launch = game.pointList[Game.generateChallengeId()]
            point = game.generatePoint()
            print(f" {launch[0]}  {point} ")
            # print(str(player[1])+' '+str(point)+' ' + str(launch[2])+' ' + str(launch[1]))
            player[1] = player[1] - ((point * launch[2]) + launch[1])
            player[1] = game.scoreNegative(player[1])
            
    @staticmethod
    def generateChallengeId():
        value = randint(0, (len(game.pointList)-1))
        return value
    
    @staticmethod
    def generatePoint():
        value = randint(1, 20)
        # try:
        #     point = int(input("Dardo :"))
        #     if point < 0 or point > 20:
        #         point = 0
        # except:
        #     point = 0
        # return point
        return value
    
    @staticmethod
    def printScore():
        header = "\n JUGADOR".ljust(16) + "PUNTOS".ljust(16)
        print(header)
        for player in PlayerHelper.playerList:
            print(f" {player[0].ljust(16)} {str(player[1]).ljust(16)} ")
    
    @staticmethod
    def scoreNegative(point):
        return abs(point)

class PlayerHelper:
    
    playerList = []
    name = "Player"
    score = 501
    
    @staticmethod
    def getPlayerName():
       
        for i in range(1,3):
            playerName = input(f"Nombre del jugador {i} :")
            playerName = PlayerHelper.replaceEmpty(playerName)
            playerName = playerName[0:3]
            playerName = PlayerHelper.upper(playerName)
            if PlayerHelper.checkElement(playerName, PlayerHelper.playerList) == False:
                PlayerHelper.playerList.append([playerName, PlayerHelper.score])
            else:
                PlayerHelper.playerList.append(
                    [playerName+'2', PlayerHelper.score])

    @staticmethod
    def checkElement(element, collection):
        # return element in collection
        return element in chain.from_iterable(collection)

    @staticmethod
    def upper(element):
        return element.upper()

    # @staticmethod
    # def replaceEmpty(collection):
    #     return [PlayerHelper.name if x == '' else x for x in collection]

    @staticmethod
    def replaceEmpty(name):
        return PlayerHelper.name if name == '' else name

game = Game()
player = PlayerHelper()

game.printWelcome()
player.getPlayerName()

while game.playGame:
    game.startGame()
