import pymongo

client = pymongo.MongoClient("mongodb://mongo:mongo@localhost:27888/")
print(client.list_database_names())
mydb = client["mydatabase"]
col = mydb["cars"]

# print("-------------------------------------------------------------\n")
# mycar = {"_id":1, "marca":"Toyoya", "modelo": "Corolla", "anio":"2011"}
# col.insert_one(mycar)
# morecars = [
#     {"_id":2, "marca":"Toyoya", "modelo": "Yaris", "anio":"2014"},
#     {"_id":3, "marca":"Mazda", "modelo": "3", "anio":"2019"},
#     {"_id":4, "marca":"Suzuki", "modelo": "Swift", "anio":"2017"}
# ]
# col.insert_many(morecars)

print("-------------------------------------------------------------\n")
print(col.find_one({"marca":"Mazda"}, {"_id":0, "modelo":0}))
print("-------------------------------------------------------------\n")
print(col.find_one({"marca": {"$gt":"P"}}, {"_id":0}))
print("-------------------------------------------------------------\n")
# regular expressions find all with the work z (db '%%' )
print(col.find_one({"marca": {"$regex":"\w*z\w*"}}, {"_id":0}))
print("-------------------------------------------------------------\n")


for x in col.find({}, {"_id":0, "modelo": 0}):
    print("El vehículo es un "+ x["marca"] +" del año "+ x["anio"])
print("-------------------------------------------------------------\n")
for x in col.find({}, {"_id":0, "modelo": 0}).limit(2):
    print("El vehículo es un "+ x["marca"] +" del año "+ x["anio"])
print("-------------------------------------------------------------\n")
for x in col.find({}, {"_id":0, "modelo": 0}).skip(2):
    print("El vehículo es un "+ x["marca"] +" del año "+ x["anio"])
print("-------------------------------------------------------------\n")
for x in col.find({"marca": {"$regex":"\w*z\w*"}}, {"_id":0}):
    print("El vehículo es un "+ x["marca"] +" del año "+ x["anio"])
    

