import pymongo
import requests
import json

client = pymongo.MongoClient("mongodb://mongo:mongo@localhost:27888/")
mydb = client["mydatabase"]

response = requests.get("https://mocki.io/v1/b294a546-1cff-4aef-aa56-42d718106461")
responseJSON = json.loads(response.text)
print(response.text)
print(responseJSON)

for t in responseJSON["Products"]:
    print(t)

col = mydb["todos"]
ret = col.insert_many(responseJSON["Products"])

response = requests.get("https://raw.githubusercontent.com/bahamas10/css-color-names/master/css-color-names.json")
responseJSON = json.loads(response.text)
print(responseJSON)

col = mydb["colors"]
ret = col.insert_one(responseJSON)