import pymongo

client = pymongo.MongoClient("mongodb://mongo:mongo@localhost:27888/")
# get all database
print(client.list_database_names())

# open db
mydb = client["mydatabase"]
# get all collection in mydatabase
print(mydb.list_collection_names())

# referent, if no exists mongo create db
mydb = client["test"]
colec = mydb["miColeccion"]

# insert one document
colec.insert_one({'x':10, 'y':20})
colec.insert_one({'x':35, 'y':80})


# mongo --username alice --password --authenticationDatabase admin --host mongodb0.examples.com --port 28015
# mongo "mongodb://mongodb0.example.com:28015"
# mongo --username mongo --paswoord mongo --host localhost --port 27888
# mongo "mongodb://mongo:mongo@localhost:27017/?authSource=admin"