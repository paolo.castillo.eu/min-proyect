# importar el conector
import mysql.connector as db
# crear una conexion con el motor mysql

mydb = db.connect(
    host='127.0.0.1',
    user='root',
    passwd='archer',
    database='ayudantia',
    auth_plugin='mysql_native_password'
)

# usando la conexión creamos un cursor
my_cursor = mydb.cursor()

# cargamos una variable la sentencia SQL para crear una nueva
# base de datos
# sqlsentence = "CREATE DATABASE ayudantia3"

carrera = "CREATE Table carrera ( id_carrera int NOT NULL primary key AUTO_INCREMENT, create_time DATETIME COMMENT 'create time', update_time DATETIME COMMENT 'update time', nombre VARCHAR(20))";
try:
    my_cursor.execute(carrera)
except Exception:
    print("Existe")

ayudante = "CREATE Table ayudante ( rut VARCHAR(12) PRIMARY KEY not NULL,create_time DATETIME COMMENT 'create time',update_time DATETIME COMMENT 'update time',nombre VARCHAR(20),carrera INT,FOREIGN KEY (carrera) REFERENCES carreras(id_carrera))";
try:
    my_cursor.execute(ayudante)
except Exception:
    print("Existe")

# ejecutamos la sentencia que crea la base de datos
# my_cursor.execute(sqlsentence)



