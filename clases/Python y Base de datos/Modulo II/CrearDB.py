import mysql.connector as db

# db create
mydb = db.connect(
    host = 'localhost',
    user = 'root',
    passwd = 'archer',
    auth_plugin = 'mysql_native_password'
)

my_cursor = mydb.cursor()

query = "CREATE DATABASE IF NOT EXISTS newDataBase"

my_cursor.execute(query)

# db table
mydb = db.connect(
    host = 'localhost',
    user = 'root',
    passwd = 'archer',
    database = 'newDataBase',
    auth_plugin = 'mysql_native_password'
)

my_cursor = mydb.cursor()

query = "CREATE TABLE IF NOT EXISTS users (name varchar(45), email varchar(45), \
    age integer(10), user_id integer auto_increment PRIMARY KEY)"

my_cursor.execute(query)

query = "INSERT INTO users (name, email, age) VALUES (%s, %s, %s)"
row = ('archer', 'archer@example.com', 25)

rows = [
 ('Hugo', 'hugo@gmail.com', 25),
 ('Paco', 'paco@gmail.com', 26),
 ('Luis', 'luis@gmail.com', 27)
]

#my_cursor.execute(query, rows)
#my_cursor.executemany(query, rows)

mydb.commit()


query = "SELECT * FROM users"
my_cursor.execute(query)
rows = my_cursor.fetchall()
# fetchone() => one row
# fetchmany(2) => count rows
for row in rows:
    print(row)


mydb.close()

