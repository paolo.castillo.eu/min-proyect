# Concepto
## ¿ Que es una base de datos ?

- set datos : un conjunto de datos estatico [dataset] -> archivo csv, excel (foto de información)

## base de datos: 
- es dinamica, puede agregarse o eliminarse muy rapida mente,
- se puede moficarse
- puede hacerse consultas complejas sobre la información en ella

## Motor de base datos
- es programa de software que opera una base de datos
- genera privilegios para crear, eliminar, modificar

## Modelo de datos
- un modelo de datos es una abstracción 
- una representación de dlos datos que permite razonar en forma mas sencilla
- un modeleo generico es una suerto de pagina
- relacional y no relacional

## Modelo relacional
- todos son tablas
- clave primaria permite identificar en forma unica una fila
- la clave foranea relaciona un atributo de una tabla con clave primaria de otra

## Modelo no relacional
- un documento es mas flexible que una tabla
- no todos los documentos deben tener la misma estructura.

# Tablas y esquema

- Tabla : una tabla es elemento que contiene una información (es dinamica).
- Esquema: es su estructura formada por atributos, clave e indice.

## Clave primaria y foreanea
- Clave Primaria : identificado de un item en la tabla
- la db construye una estructura de acceso eficiente para rescatar la información acorde a un valor
- porque no declara cada atributo como indice secundario ? - implica mayor costo
- Clave Foreanea : permite asocia un item de una tabla con item de otra tabla.

- Bajo un modelo relacional, la información esta contenida solamente en tablas.
- Cada tabla contiene un numero filas donomidad tuplas, siendo todas diferentes.
- Cad tupla contiene distintos aspectos de información respecto a una instancia o item.
- El motor construye una estructura de acceso que permite llegar rapidamente a la tupla.
- Una atributo cuyos valores permiten identificar por si solo a la tupla que contiene se conoce como clave primaria.
- La clave primaria puede ser tambien un conjunto de atributos.
- Se conecta una tabla sobre otra mediante claves foráneas
- En cocaciones es necesario designar atributos de una tabla como indices secundarios, casos en los que el motor construye estructura de acceso adicionales para acceso adicionales para acceder a tupla desde esos atributos
- Existen indices secundario puede acelerar las consultas, pero al mismo tiempo hacer mas lentas las actualizaciones de información.