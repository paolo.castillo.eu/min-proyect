# NoSQL

## ¿ Por que base de datos NoSQL ? (Más bien no relacional)
- El movimiento NoSQL, nacen como respuesta a escenarios donde la necedidad de usar una DB distribuida impacta significativamente el rendimiento de motores relacionales (Facebook, Twitter).
- Son capaces de manejar información semiestructurada (modelo de datos mas flexibles).
- No hay una idea fuerte de esquema.
- Por lo general se relejan las exigencias sobre transacciones ACID.
- capacidad de escalimento mejor

### Debilidades del modelo relacional
- No requiere de todas las funcionalidades de un sistema de gestión de base de datos.
- Soluciones más simples tienen el potencial de mejor desempeño.
- RDBMS son complejos de escalar cuando hay que replicar servidores (horizontal scaling), dificultando desplegar soluciones de alta disponiblidad. (distribuir la información).

## Tipo de motores NoSQL
- En común son no relacionales

## Modelos de grafos (charts)
(no confundir con graficos)
Apropiada para datos fuertemente interconectados (Ej: mapas):
- Relaciones e interacciones entre personas
- Problemas de redes y flujos
- Problemas de rutas
- La web (page rank)

## Modelo de documentos (la vida real se expresa en base documento) (MongoDB)
- Documentos expresados en formatos XML o JSON.
- Es menos estructurado que una tabla:
    - Cada documento representa un objeto completo (factura, ficha medica, ley).
    - No requiere comprometerse a priori con un esquema.
- La búsqueda se basa en una clave primaria o atributos escogidos del documento.
- Dentro de los motores más usados destacan:
    - CouchDB
    - Mongo

### Ventajas
- El mundo funciona con documentos, como rentas, formularios de impuestos, cartas, memos.
- Un mismo tipo de documento no implica exactamente la misma estructura:
    - Ficha clinica.
    - Ley de la replublica.
- No es necesario dividir las entidades para ser almacenadas eficientes:
    - Factura en lugar de encabezados y detalle.
    - Todas la información de un empleado en un solo documento.

Ej:
FACTURA: 1222001
20/12/2019

ACM INC.
APOQUINDO 345, LAS CONDES, SANTIAGO
10  2352    UNION T 1500    15.000
6   1200    SUPIE R 2100    12.600
20  3200    JUNTA   3200    64.000

```
{
    "Numero":"122001",
    "Fecha":"2019-12-20",
    "Empresa":"ACM",
    "Direccion": {
        "Calle":"APOQUINDO 345",
        "Comuna":"LAS CONDES",
        "Ciudad":"SANTIAGO"
    }
    "Detalle": [
        {"Cantidad":"10","Producto":"2352"},
        {"Cantidad":"6","Producto":"1200"},      
        {"Cantidad":"20","Producto":"3200"},      
    ]
}
```

## Modelo de columna anchas (google cloud table)
## Modelo de valor (raiz)

## Síntesis
- El movimiento NoSQL surge como respuesta a nuevos escenarios:
    - Necesidad de manejar enormes volúmenes de datos.
    - Necesidad de manejar información semiestructurada.
- No existe un unico tipo de modelo.
- No existe motores de DB que soportan estos modelos.
- El modelo de documentos implementado en el motod de MongoDb es el más popular de este tipo.
- Los documentos en MongoDB puede reflejar fielmente los documentos reales.

# MONGODB

## Principales características del motor
- Motor OpenSource.
- Alto desempeño de la comunidad.
- Tipo de escenario en que es interesante.
- Soporta el modulo de documento.
    - BSON es un formato binario de los documentos JSON.
    - Se diferencia en que los documentos son almacenados en forma binarias, lo que hace más rapido y eficiente.

```
Para todos los efectos funciona igual que si estuviésemos manejando
JSON:
- Objetos como secuencia de pares nombre/valor.
- Los valores puedes:
    - Ser otros objetos
    - Ser arrays
    - Combinar objetos y arrays
```

- No hay soporte de transacciones
    - Compromiso usual en motores Nosql para logar escalamiento horizontal.
    - Mysql no tuvo soporte de transacciones por un largo tiempo
    - Asegura durabilidad

### Objetos y colecciones reemplazan a filas y tablas (conjunto de colecciones)
- En mongoDB un documento es una unidad de almacenamiento:
    - Equivalente a la final de una tabla, pero ahora puede ser de complejidad arbitraria.
    - Cada documento tiene un identificador único llamado _id.
    - Una colección es una caja de documentos con una etiqueta.
    - Es equivalente a una tabla, pero los documentos no tiene por qué ser todo iguales.
- Una base de datos de MongoDB es un conjunto de colecciones.

### Varias formas posibles
- Directamente desde la linea de comando
- Usar interfaces gráficas(compas)
- Desde un programa(python)

#### Línea de comando
- Se invoca el intérprete de comando al ejecutar Mongo.
- Comandos
    - show dbs
    - use database
    - show collection

## Interacción con mongoDB
```
$ mongo
> show dbs
admin       0.000GB
config      0.000GB
local       0.000GB
mydatabase  0.000GB

> use mydatabase
switched to db mydatabase

> show collections
amigos
customers

> db.createCollection("otracoleccion")
{"ok" : 1}

> show coleccionts
amigos
customers
otracoleccion

> db.otracolecion.insert({nombre: "jaime", apellido: "navaro"})
writeresult {("ninserted":1)}

> db.otracoleccion.find()
{ "_id": ObjectId("5e028a4c63bd15clbb53110b"),
"nombre": "jaime", "apellido": "navaro" }

# Agregando documentos a una colección
>db.otracoleccion.insertOne()
>db.otracoleccion.insertMany()

# devolver toda la colección
> db.otracoleccion.find()
```

## Buscando información en mongoDB
- La operación para buscar es find()
    - find() sobre una colección extra todos los documentos de ella:
    `db.coleccion.find()` - todos los documentos en la colección
    - Puede acotarse a los documentos que cumplen con ciertas características, incluyendo un parámetro:
    `db.coleccion.find( {name:"jaime"} )` - solo el documento con nombre = "jaime"
    - También es posible extraer solo algunos atributos de los documentos:
    `db.coleccion.find( {name:"jaime"},{nota:1} )` - solo la notas del documento que tiene nombre = "jaime"
    - Para extraer solo un documento se usa findOne
    db.coleccion.findOne() - solo trae un documento
    - `count()` devuelve el número de documentos
    - `distinct()` permite eliminar repetidos

```
> db.alumnos.find()
...
> db.alumnos.findOne()
{
    "_id":1,
    "name": "Alberto",
    "notas": {
        "pythondb": 6.5,
        "pythonweb": 5.5
    }
}
# busca toda la colección con nombre jaime
> db.alumnos.find({name:"jaime"})
{ "_id": 2, "name": "jaime", "notas": {"pythondb": 6.5,"pythonweb": 5.5} }
# buscar las notas con nombre jaime
> db.alumnos.find({name:"jaime"}, {notas:1})
{ "_id": 2,"notas": {"pythondb": 6.5,"pythonweb": 5.5} }

# distinguir en base al nombre
> db.alumnos.distinct("name")
{"alberto", "jaime", "joana", "luis", "ema"}
# contar nombres
> db.alumnos.count()
5
```

### Operadores
- $gt - greater than
- $gte - greater than or equal
- $lt - less than
- $lte - less than or equal
- $ne - not equal
- $in - in array
- $nin - not in array

```
# aparescan con j o mayor a j en abecedario
> db.alumnos.find({name: {$gt: "J"}}, {name:1})
{"_id":1, "name":"Jaime"}
{"_id":2, "name":"Joana"}
{"_id":3, "name":"Luis"}
{"_id":5, "name":"Jaime"}

> db.alumnos.find({name: {$ne: "Jaime"}})
{"_id":1, "name":"Alberto", "notas": {}}
{"_id":2, "name":"Joana", "notas": {}}
{"_id":3, "name":"Luis", "notas": {}}
{"_id":5, "name":"Ema", "notas": {}}

```

### Uso de expresiones regulares
- En SQL vimos la utilidad de los comodines % y _ para especificar en forma parcial una secuencia de caracteres.
- En mongoDB se potencia enormente al permitir usar expresiones regulares(regex).
- Ejemplo:
    - La regex `/^jai/i` especificar cualquier secuencia que comienza con la letra "jai" sin importar si son mayúsculas o minúsculas.
    - `/^[AE]` una secuencia que comience con A o con E
    - `/Jo\wn\w/` una secuencia en que aparezca una secuencia del tipo jo_n_

```
> db.alumnos.find({name: /^jai/i})
{"_id":1, "name":"jaime", "notas": {}}

> db.alumnos.find({name: /^[AE]})
{"_id":1, "name":"alberto", "notas": {}}
{"_id":5, "name":"ema", "notas": {}}

> db.alumnos.find({name: /Jo\wn\w/})
{"_id":2, "name":"joana", "notas": {}}

```

# Síntesis
- MongoDB es un motor de BD que implementa eficientemente el modulo de documentos.
- El formato de los documentos es BSON, una forma binaria de JSON.
- Se puede interactuar con el motor desde la línea de comando, desde la GUI o desde un programa.
- Hay ppoderoasas formas de buscar elementos almacenados en la base de datos.

# El formato json

## ¿Que es JSON?
- Es un estándar de almacenamiento de intercambio.
- JSON viene del mundo de javascript (javascript object notation)
    - Conjunto de pares propiedad: valor, delimitados por {}.
    - Los valores pueden ser:
        - Tipo simple: number, string boolean, null
        - Arreglos: lista del tipo `['a','b']`
        - otros objetos
Ejemplo:
```
{
    "menu" : {
        "id":"file",-- value
        "value":"file", -- value
        "popop":{ -- objetos
            "menuitem":[ -- arreglo
                {"value":"new", "onclick":"create()"},
                {"value":"open", "onclick":"open()"},
                {"value":"close", "onclick":"close()"},
            ]
        }
    }
}
```

## METADATA
### Modelo Relacionarl
- Información sobre los datos.
- Modelo relacional.
    - El esquema de una tabla almacena la metadata.
    - Cada fila de la tabla adscribe a la misma metadata.
- Cada dato en cada tabla tiene un significado preciso, relacionado con el esquema de la tabla.
- Basta saber en qué columna está el dato.

### Modelo documentos
- Cada documento guarda su propia metada.
- Permite que podamos tener distintos documentos en una misma colección.
- Cada documento acarrea toda su metadata.
- Es menos eficiente si la data es estructurada y hay solo un esquema.
- Es más flexible ya que posibilita manejar objetos (filas) con más elementos o elementos diferentes.

## Sintaxis de JSON
- Estructura nombre:valor
    -   string, numeros, arrays, objetos

Ejemplo:
```
{
    "empleado":[
        {"nombre":"juan", "apellido":"flores"},
        {"nombre":"ana", "apellido":"frank"},
        {"nombre":"alexis", "apellido":"sanchez"}
    ]
}

{
    "encabezado":{
        "numfactura":"33",
        "rut":"99581150-2",
        "direccion":{
            "calle": "apoquindo 5544",
            "comuna": "las condes",
            "ciudad": "santiago"
        },
        "detalle":[
            {
                "nmbtem":"item 1", "dscltem": "descripcion del item 1", "qtyitem":"3"
            },
            {
                "nmbtem":"item 2", "dscltem": "descripcion del item 2", "qtyitem":"5"
            }
        ]
    }
}
```

# Acceso al contenido

- En el ejemplo la variable perosna contiene un objeto con nombre y apellido
```
persona={"nombre":"juan", "apellido":"flores"}
```
- Para acceder al apellido de ese objeto se puede hacer de dos formas
```
persona.apellido
persona[apellido]
```

# Síntesis
- JSON es un formato estándar muy simple y extramadamente flexible.
- Un objeto json permite representar lo que podría ser una fila de una tabla.
- Permite también, representar objetos complejos que contienen otras estructuras o arreglos.
- Cada documentos JSON contiene su propia metadata.
