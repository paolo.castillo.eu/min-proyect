# Instalación mediante PIP
```
$ pip install pymongo
```

# Conectando con MongoDB
1. Importar el módulo `pymongo` (debe estas disponible en el ambiente anaconda)
2. Generar un objeto cliente invocando el método MongoClient de pymongo.
    - Se debe pasar como parámetro el string de conexión, por ejemplo "mongodb://localhost:27017"
    - Se puede obtener una lista de la bases de datos en el motor con `cliente.list_database_name()`.
3. Obtener una referencia a la base de datos que nos iteresa usar con cliente `[base_de_datos]`.
4. Desde allí se puede hacer todo tipo de cosas con la base de datos por ejemplo, listar todas las colecciones disponibles con `db.list_collection_names`.


Ejemplo:
```
import pymongo
client = pymongo.MongoClient("mongodb://localhost:27017")
print(client.list_database_name())
mydb = client["mydatabase"]
print(mydb.list_collection_name())
```

```
# lista de la base de datos disponible
["admin","config","loca","mydatabase"]
["amigos","alumnos","customers","otracoleccion"]
```

# Agregando información a la base de datos
La referencia al cliente y la referencia a la base de datos operan como diccionarios:
`miDB = client["alguna_db"]`
- Si `alguna_db` no existe se creará (solo cuando agreguemos algo 
realmente se hará efectivo)

`miColeccion = miDB["nuevo_coleccion"]`
- Si `nuevo_coleccion` no existe se creará (solo cuando agreguemos algo realmente se hará efectivo).

Para insertar un nuevo documento en una colección se invoca insert_one() o inser_many() sobre la referencia a la colección.
En el primer caso se le pasa un objeto JSON, en el segundo una lista de objetos JSON.

# Find es el select de mongoDB
- `find_one()` - devuelve el primer documento de la colección.
- `find()` -  devuelve todos los documentos de la colección. 
- find puede tomar dos parámetros
    - una consulta (query objet) la estructura de la respuesta (response objet).

El objeto respuesta es un JSON con pares llave valor, en que las llaves corresponde a las de los documentos de la colección y los valores son 0 o 1 dependiendo si uno quiere que apareszan o no:

`con.find_one({"_id":0, "marca":1}) = {"marca":"toyota"}`

`con.find_one({"_id":0, "marca":1, "modelo": 1}) = {"marca":"toyota", "modelo": "yaris"}`

# Seleccionando documentos en el find
- El primer argumento del find se interpreta como la consulta.
- Puede aparecer pares llave:valor
- El valor puede aparecer:
    - Constantes
    - Operadores de comparación ($gt, $lt, $ne, etc)
- Una expresión regular ($regex)

```
> find_one({"marca":"mazda"},{"_id":0})
    {"marca":"mazda", "modelo": "3", "agno":"2015"}
> find_one({"marca":{"$gt":"P"}},{"_id":0}) # gt great than (mayor a que)
    {"marca":"toyoya", "modelo": "yaris", "agno":"2013"}
> find_one({"marca":{"$regex":"\w*z\w*"}},{"_id":0}) # 
    {"marca":"mazda", "modelo": "3", "agno":"2015"}
```

# Consultar que entregan más de un documento

Un find puede devolver muchos documentos por loq ue deben ser extraídos con una estrcutura for

- Un find
```
> for x in col.find({}, {"_id":0, "modelo":"0"}):
>    print(x)
{"marca": "toyota", "agno":"2013"}
{"marca": "mazda", "agno":"2015"}
{"marca": "toyota", "agno":"2011"}
```
- Puede usarse limit(n) para limitar el números de documentos
```
> for x in col.find({}, {"_id":0, "modelo":"0"}).limit(2):
>    print(x)
{"marca": "toyota", "agno":"2013"}
{"marca": "mazda", "agno":"2015"}
```
- Puede usarse skip(n) para saltarse los primeros
```
> for x in col.find({}, {"_id":0, "modelo":"0"}).skip(2):
>    print(x)
{"marca": "toyota", "agno":"2011"}
```

# Extracción desde una API JSON

Los objetos JSOn que entregan la API pueden ser insertados directamente en una colección de Mongo. Además de pymongo se debe importar request.

`response = get("https://....")`
- Junto con ello se debe importar libreria JSON:
`doc = json.loads(response.text)`
- Los docs recibidos simplemente se insertan en la colección deseada:
```
> for documento in docs:
>   docCol.insert_one(documento)
```

# Síntesis
- Para conectar un programa Python con un motor de BD mongo se de generar primero, un cliente por medio del modulo pymongo.
- Con el cliente se puede crear una nueva BD o conectarse a una existente.
- Los objetos JSON se agregan a colecciones determinadas mediante insert_one o insert_many.
- Para consultar información se usa la poderosa operación `find()` o `find_one()`.
- `find` lleva dos parámetros de los cuales el primero sirve para especificar el filtro y el segundo para especificar que partes de los documentos nos interesan.
