import mysql.connector as db

mydb = db.connect(
    host = 'localhost',
    user = 'root',
    passwd = 'archer',
    database = 'ayudantia',
    auth_plugin = 'mysql_native_password'
)

cursor = mydb.cursor()
# query = "INSERT INTO clientes (contacto, nombre_cliente) VALUES (%s, %s)"
# rows = [
#         ["12321", "paolo"],
#         ["56346", "mario"],
#     ]
# cursor.executemany(query, rows)
# mydb.commit()

query = "SELECT * FROM clientes"
cursor.execute(query)
rows = cursor.fetchall()
print("=== duplas agregadas ===")
for row in rows:
    print(row)

query = "DELETE FROM clientes WHERE clienteid = 5"
# query = "DELETE FROM clientes WHERE clienteid LIKE '%mario' --termina"

cursor.execute(query)
mydb.commit()

query = "SELECT * FROM clientes"
cursor.execute(query)
rows = cursor.fetchall()
print("=== despues de eliminar ===")
for row in rows:
    print(row)
