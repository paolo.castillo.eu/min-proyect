# Pandas
es una libreria mas grandes fortalezas del lenguaje python es la disponibilidad de muy buenas librerias que permiten ahorrar muchisimo trabajo de programación.

- Dataframa como estrcutura general para el manejo de datos.
- herramientas para leer archivos en distintos formatos.
- herramientas para recortar (slicing) los dataframes de la mas variadad disponible.

## Dataframes

### Estructura similar a una tabla:
- cada fila tiene un indice
- el indice tambien podria tener un nombre
- puede crearse a partir de un archivo csv de un diccionario python.

```
data = {
    'hombre' : [47, 64, 51],
    'mujeres' : [32, 25, 49]
}

miDataFrame = pd.DataFrame(data)
miDataFrame = pd.DataFrame(data, index=['mat121', 'mat122'])
```

### Referencia datos especificos en el data frame

#### operadores

`- loc - iloc`
```
miDataFrame.loc['mat121']
miDataFrame.iloc[1]
```

seleccionar columnas de un dataframe df para generar uno nuevo: `df[[col1, col2]]`
seleccionar primera fila: `df.iloc[0,:]`
seleccionar la segunda columna completa: `df.iloc[:,1]`
seleccionar la primeras dos filas: `df.iloc[0:2]`

