import mysql.connector as db

mydb = db.connect(
    host = 'localhost',
    user = 'root',
    passwd = 'archer',
    database = 'ayudantia',
    auth_plugin = 'mysql_native_password'
)

cursor = mydb.cursor()

query = "SELECT * FROM clientes"
cursor.execute(query)
rows = cursor.fetchall()
print("=== duplas agregadas ===")
for row in rows:
    print(row)

# query = "UPDATE clientes SET contacto = 123123 WHERE clienteid = 6 "
query = "UPDATE clientes SET nombre_cliente = 'Mario Sepulveda' WHERE nombre_cliente = 'mario' "

cursor.execute(query)
mydb.commit()

query = "SELECT * FROM clientes"
cursor.execute(query)
rows = cursor.fetchall()
print("=== despues de eliminar ===")
for row in rows:
    print(row)