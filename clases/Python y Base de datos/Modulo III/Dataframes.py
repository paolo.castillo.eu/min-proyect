import os
import pandas as pd
import mysql.connector as db

mydb = db.connect(
    host = 'localhost',
    user = 'root',
    passwd = 'archer',
    database = 'ayudantia',
    auth_plugin = 'mysql_native_password'
)

# leer archivo
file = os.path.join(os.path.dirname(__file__), "personas.csv") 
df1 = pd.read_csv(file, sep=";")
df2 = df1[['nombre', 'rut', 'fecha_nac']]

print(df1)
print(df2)

# slicing del dataframes
print(df2.iloc[0:4, 0:1])
print(df2.loc[5:7, ["rut", "nombre"]])

# Leyendo desde Mysql y poniendo el resultado en un dataframe
cursor = mydb.cursor()
query = "SELECT * FROM Consumos WHERE tcal > 800 AND tcal < 900"
cursor.execute(query)

rows = cursor.fetchall()

list = list()

for row in rows:
    list.append(row)

df3 = pd.DataFrame(list, columns=['id', 'Año', 'Tipo', 'Actividad', 'SubActividad', 'Region', 'Consumo'])
print(df3.loc[:, ['Año', 'Tipo', 'Region', 'Consumo']])

print(df3.values.tolist())
tuple_list = [tuple(l) for l in df3.values.tolist()]

cursor.execute("DELETE FROM Consumos")
mydb.commit()

sqlSentece = "INSERT INTO Consumos(id, agno, tipo, actividad, subactividad, region, tcal) \
    values (%s, %s,%s,%s,%s,%s,%s)"
cursor.executemany(sqlSentece, tuple_list)
mydb.commit()