class Persona:
    
    def __init__(self, nombre):
        self.nombre = nombre

class Cliente(Persona):

    def __init__(self, nombre, exigencia, distancia, plato_preferido):
        super().__init__(nombre)
        self.exigencia = exigencia
        self.distancia  = distancia
        self.plato_preferido = plato_preferido

    def recibir_pedido(self, pedido, demora):
        clasificacion = self.clasificacion
        if demora >= 5 and self.exigencia != "Leve":
            clasificacion  /= 2
        for plato in pedido:
            if self.exigencia == "Inmensa" and plato.calidad >= 8:
                clasificacion += 3
            elif self.exigencia == "Mediana" and plato.calidad >= 5 and plato.calidad <= 7:
                clasificacion += 2
            elif self.exigencia == "Leve" and plato.calidad >= 1 and plato.calidad <= 4:
                clasificacion += 1
                
class Cocinero(Persona):

    def __init__(self, nombre, habilidad):
        super().__init__(nombre)
        self.habilidad = habilidad
    
    def cocinar(self, plato):
        pass


class Repartidor(Persona):

    def __init__(self, nombre, velocidad):
        super().__init__(nombre)
        self.velocidad = velocidad

    def repartir(self, pedido, distancia):
        pass
