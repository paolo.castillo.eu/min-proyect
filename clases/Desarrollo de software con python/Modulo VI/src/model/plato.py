from random import randrange

class Plato:
    
    def __init__(self, nombre):
        self.nombre = nombre
        self.calidad = 0
        self.dificultad = randrange(1, 10)

class Bebestible(Plato):
    
    def __init__(self, nombre):
        super().__init__(nombre)
        self.tamanio = 500

class Comestible(Plato):

    def __init__(self, nombre, cubierto = None):
        super().__init__(nombre)
        self.cubierto = cubierto

if __name__ == '__main__':
    plato1 = Bebestible("Jugo", )
    plato2 = Comestible("Lomo a lo pobre")
    plato3 = Bebestible("Pisco Sour")
    plato4 = Comestible("Empadas")
    bebestible = [plato1, plato3]
    comestible = [plato2, plato4]
    for plato in bebestible:
        print(f"Nombre: {plato.nombre}, Tamaño: {plato.tamanio}, dificultad: {plato.dificultad}")
    for plato in comestible:
        print(
            f"Nombre: {plato.nombre}, Tamaño: {plato.cubierto}, dificultad: {plato.dificultad}")
