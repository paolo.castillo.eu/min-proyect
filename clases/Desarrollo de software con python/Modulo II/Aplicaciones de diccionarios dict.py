# Aplicaciones de diccionarios dict

# convertir monedas a cualquier tipo

pesos = int(input("pesos: "))
moneda = input("moneda destino: ")
if moneda == 'EUR':
    print(f"{pesos} pesos --> {pesos/913.33} EURO")
elif moneda == 'USD':
    print(f"{pesos} pesos --> {pesos/819.33} DOLAR")
elif moneda == 'GBP':
    print(f"{pesos} pesos --> {pesos/1054.33} LIBRA")
elif moneda == 'PEN':
    print(f"{pesos} pesos --> {pesos/239.33} Sol")
elif moneda == 'BOB':
    print(f"{pesos} pesos --> {pesos/118.33} Bolivares")
else:
    print(f"Conversión desconocida")

monedas = {
    'EUR' : ('EURO', 913.33 ),
    'USD' : ('DOLAR', 819.33 ),
    'GBP' : ('LIBRA', 1054.33 ),
    'PEN' : ('Sol', 239.33 ),
    'BOB' : ('Bolivares', 118.33 ),
}

pesos = int(input("pesos: "))
m = input("Moneda destino: ")
if m in moneda:
    print(f"{pesos} pesos ---> {pesos/monedas[m][1]} {monedas[m][0]}")
else:
    print("Conversión desconocida")

# pesos: 1000
# moneda destino: USD
# 1000 pesos - -> 1.2205094406405232 DOLAR
# pesos: 1000
# Moneda destino: USD
# 1000 pesos - - -> 1.2205094406405232 DOLAR

# Frecuencia de palabras repetidas
# conteo de repeticiones de palabras
# diccionario permite busuqeda eficiente a palabra existentes

texto = '''El amor es un mejor profesor que el deber información no 
es conocimiento. nunca pierdas la sagrada curiosidad. si no puedes 
explicarlo de forma, no lo entiendes lo suficientesmente bien. todos deben ser
respetados como individuos, pero no idolatrados.'''

repeticiones = dict()
palabras = texto.lower().replace(',','').replace('.','').split()
for palabra in palabras:
    if palabra in repeticiones:
        repeticiones[palabra] += 1
    else:
        repeticiones[palabra] = 1
        
print(repeticiones)

# {'el': 2, 'amor': 1, 'es': 2, 'un': 1, 'mejor': 1, 'profesor': 1, 'que': 1, 'deber': 1, 'información': 1, 'no': 4, 'conocimiento': 1, 'nunca': 1, 'pierdas': 1, 'la': 1, 'sagrada': 1, 'curiosidad': 1, 'si': 1,
#    'puedes': 1, 'explicarlo': 1, 'de': 1, 'forma': 1, 'lo': 2, 'entiendes': 1, 'suficientesmente': 1, 'bien': 1, 'todos': 1, 'deben': 1, 'ser': 1, 'respetados': 1, 'como': 1, 'individuos': 1, 'pero': 1, 'idolatrados': 1}

# registro de estudiantes
# consultas a un registro

curso = {
    '972-1' : ('Aurora', [67, 56, 48]),
    '580-2' : ('sebastian', [21, 60, 38]),
    '632-7' : ('rafaela', [53, 72, 22]),
    '600-4' : ('dario', [61, 1, 42])
}

# calcular promedio de terceros puntajes de cada estudiante
# values() permite recorrer cada valor del diccionario
prom = 0
for v in curso.values():
    prom += v[1][2]
prom = prom / len(curso)
print(f"El promoedio del segundo puntaje es {prom}")
# El promoedio del segundo puntaje es 37.5

# consultar a un registro estudiante
codigo = input("consultar por alumno: ")
estudiante = curso[codigo]
print(f"Estudiante {codigo}. nombre {estudiante[0]}, y puntaje {estudiante[1]}")
# Estudiante 972-1. nombre Aurora, y puntaje [67, 56, 48]
