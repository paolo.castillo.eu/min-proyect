# Diccionarios y operaciones
# almacenar información no secuencial
# elementos son llaves y cada uno tiene asociado un valor

# estructura dict()

diccionarios = {}
diccionarios = dict()

print(diccionarios) # {}
print(type(diccionarios)) # <class 'dict'>

profesores = {'PBD','jaime','DSP','cristian', 'VBD', 'denis'}
diccionarios_vacios = {}
mezcla= {'pais': 'chile', 8:4.15, (4,20020): ['abril','anio']}
subdict = {15:('arica',9), 'd': mezcla}

print(profesores) # {'cristian', 'jaime', 'denis', 'VBD', 'PBD', 'DSP'}
print(diccionarios_vacios) # {}
print(mezcla) # {'pais': 'chile', 8: 4.15, (4, 20020): ['abril', 'anio']}
print(subdict) # {15: ('arica', 9), 'd': {'pais': 'chile', 8: 4.15, (4, 20020): ['abril', 'anio']}}

# longitud de un conjunto
mezcla = {'chile': 8, 4.15:'abril', (3,5):['2020','mooc']}
largo = len(mezcla)
print(largo)

# ¿ que valor esta asociado k ?

print(mezcla[(3,5)]) # 3
print(mezcla['chile']) # ['2020', 'mooc']
print(mezcla[4.15]) # abril
print(mezcla[(3, 5)][1])  # mooc
print(mezcla[4.15][2]) # r

# modificar sobre diccionarios

# agregar elemento
mezcla['l'] = 'lunes'
print(mezcla)

mezcla[(3,5)] = 8 # {'chile': 8, 4.15: 'abril', (3, 5): ['2020', 'mooc'], 'l': 'lunes'}
print(mezcla) # {'chile': 8, 4.15: 'abril', (3, 5): 8, 'l': 'lunes'}

mezcla = {'chile': 8, 4.15:'abril', (3,5):['2020','mooc']}

# existe la llave
print('chile' in mezcla)  # True
print(2 in mezcla)  # False
# print(mezcla[2]) Error

# eliminar un elementos pop
v = mezcla.pop(4.15)
print(v)  # abril
print(mezcla)  # {'chile': 8, (3, 5): ['2020', 'mooc']}

# recorriendo diccionarios
mezcla = {'chile': 8, 4.15: 'abril', (3, 5): ['2020', 'mooc']}

# recorrer pares
for k,v in mezcla.items():
    # chile -> 8
    # 4.15 -> abril
    # (3, 5) -> ['2020', 'mooc']
    print(f"{k} -> {v}")

# recorrer llaves
for k in mezcla.keys():
    # chile
    # 4.15
    # (3, 5)
    print(k)

# recorrer valores
for v in mezcla.values():
    # 8
    # abril
    # ['2020', 'mooc']
    print(v)
