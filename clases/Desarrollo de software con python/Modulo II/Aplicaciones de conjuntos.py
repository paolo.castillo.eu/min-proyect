# Aplicaciones de conjuntos

verano = set(['acelga', 'apio', 'espinaca', 'repollo', 'lechuga'])
otonio = set(['acelga', 'ajo', 'cebolla', 'lechuga', 'papa'])
invierno = set(['pimenton', 'acelga', 'aji', 'espinaca', 'lechuga'])
primavera = set(['albahaca', 'acelga', 'cebolla', 'lechuga', 'tomate'])

# ¿ cuantas plantas disintas hay ?
# ¿ cuantas plantas disintas hay ?
# ¿ Que plantas son exclusivas de primavera y verano ?

# ¿ cuantas plantas disintas hay ?
plantas = verano | otonio | invierno | primavera # union
print(plantas)
# {'repollo', 'espinaca', 'acelga', 'tomate', 'lechuga', 'albahaca', 'cebolla', 'papa', 'aji', 'ajo', 'pimenton', 'apio'}
print(len(plantas))
# 12

# ¿ cuantas plantas disintas hay ?

todoelanio = verano & otonio & invierno & primavera
print(todoelanio)
# {'lechuga', 'acelga'}

# ¿ Que plantas son exclusivas de primavera y verano ?

priver = (primavera | verano) - otonio - invierno
print(priver)
# {'apio', 'tomate', 'repollo', 'albahaca'}

# Eliminacion de duplicados

texto = '''El amor es un mejor profesor que el deber información no 
es conocimiento. nunca pierdas la sagrada curiosidad. si no puedes 
explicarlo de forma, no lo entiendes lo suficientesmente bien. todos deben ser
respetados como individuos, pero no idolatrados.'''

lista = texto.lower().replace(',','').replace('.','').split()
print(lista)
conjunto = set(lista)
print(f"palabra en lista : {len(lista)}")  # 39
print(f"palabra en conjunto: {len(conjunto)}") # 33


# conjunto de eficiente en un conjunto vs lista (evidencia empirica)
# list vs set

from time import time

ELEMENTOS = 10 ** 7
ELEMENTOS_A_BUSCAR = ELEMENTOS // 2
lista_gigante = list(range(ELEMENTOS))
set_gigante = set(range(ELEMENTOS))

start_time = time()
ELEMENTOS_A_BUSCAR in set_gigante
finish_time = time()
set_time = finish_time - start_time
print(f"set: Búsqueda de {ELEMENTOS_A_BUSCAR} demora... {set_time:.6f} segs")
start_time = time()
ELEMENTOS_A_BUSCAR in lista_gigante
finish_time = time()
list_time = finish_time - start_time
print(f"list: Búsqueda de {ELEMENTOS_A_BUSCAR} demora... {list_time:.6f} segs")
# print(f"búqueda en el set fue {list_time/set_time:.2f} mas rápida que lista")

# conjunto es ma rapida
# set: Búsqueda de 5000000 demora... 0.000000 segs
# list: Búsqueda de 5000000 demora... 0.053856 segs