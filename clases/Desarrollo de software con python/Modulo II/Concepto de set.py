# Concepto de set

# Con junto de elementos.
# no secuencial. no hay orden entre elementos.
# elementos almacenados una unica vez.

# Estructura set

conjunto = set() 
print(conjunto)  # set()
print(type(conjunto))  # <class 'set'>

profesores = {'jaime', 'cristian', 'denis'}
conjunto_vacio = set()
mezcla = set(['chile', 8, 4.15, 'abril'])
subtupla = {15, ('arica', 9), 'febrero'}

print(profesores) # {'denis', 'jaime', 'cristian'}
print(conjunto_vacio) # set()
print(mezcla) # {8, 'chile', 4.15, 'abril'}
print(subtupla) # {('arica', 9), 'febrero', 15}

# Operaciones

mezcla = {'chile', 8, 4.15, 'abril', (3,5), '2020', 'MOOC'}

# longitud de un conjunto
largo = len(mezcla)

# Existe el elemento es ?
print('abril' in mezcla) # True
print(8 in mezcla) # True
print(4.151 in mezcla) # False
print((3,5) in mezcla) # True
print((3,5,4) in mezcla) # False

# modificar elementos:
# sets no son indexables

# mezcla = {'chile', 8, 4.15, 'abril', (3, 5), '2020', 'MOOC'}
# mezcla[3] = 'mayo'
# print(mezcla)  # 'set' object does not support item assignment

# Recorriendo conjunto
# for _ in set
mezcla = {'chile', 8, 4.15, 'abril', (3, 5), '2020', 'MOOC'}

for elemento in mezcla:
    print(elemento)
    # chile
    # 2020
    # 4.15
    # abril
    # 8
    # (3, 5)
    # MOOC
# Orden se mantiene durante el programa, pero puedo ser distincto en ejecución
# orden de los elementos puede variar al modificar el set
# garantizado que cada elemento se recorre solo una vez

# Agregar y sacar elementos

# agregar elemento con add
opciones = {'si', 'no'}
opciones.add('no se') # {'no', 'si', 'no se'}
print(opciones)
opciones.add('no')  # {'no', 'si', 'no se'}
print(opciones)


# eliminar elementos: remove, discard
cuando = {'mañana','pasado','hoy'}
cuando.remove('pasado')  # {'mañana', 'hoy'}
print(cuando)
cuando.discard('ayer')  # {'mañana', 'hoy'}
print(cuando)

# union all
# union de conjunto | union
otonio = {'abril', 'mayo', 'junio'}
invierno = {'julio', 'agosto'}
oterno = otonio | invierno # {'mayo', 'julio', 'abril', 'junio', 'agosto'}
print(oterno)
# {'mayo', 'julio', 'abril', 'junio', 'agosto'}
oterno = otonio.union(invierno)
print(oterno)

# inner join
# interseccion de elementos
# interseccion de conjunto y insection
otonio = {'mar', 'abr', 'may', 'jun'}
invierno = {'jun','jul','ago','sep'}

solsticio = otonio & invierno
print(solsticio) # {'jun'}
solsticio = otonio.intersection(invierno)
print(solsticio)  # {'jun'}

# diferenciar right join o left join
# diferencia de conjunto.- difference
otonio = {'mar', 'abr', 'may', 'jun'}
invierno = {'jun','jul','ago','sep'}

solotonio = otonio - invierno
print(solotonio)  # {'mar', 'abr', 'may'}
solotonio = otonio.difference(invierno)
print(solotonio)  # {'mar', 'may', 'abr'}


# difrencia simetrica minus
# diferencia simetrica: ^, symmetric_difference
otonio = {'mar', 'abr', 'may', 'jun'}
invierno = {'jun','jul','ago','sep'}

puros = otonio ^ invierno
print(puros)  # {'may', 'jul', 'abr', 'sep', 'ago', 'mar'}
puros = otonio.symmetric_difference(invierno)
print(puros)  # {'may', 'jul', 'abr', 'sep', 'ago', 'mar'}

# comparaciones: > , >=, <, <=, ==, disjoint
otonio = {'mar', 'abr', 'may', 'jun'}
invierno = {'jun', 'jul', 'ago', 'sep'}
primavera = {'sep', 'oct', 'nov', 'dic'}
oterno = otonio | invierno

print(oterno >= invierno) # True
print(oterno == {'mar', 'abr', 'may', 'jun', 'jun', 'jul', 'ago', 'sep'}) # True
print(otonio.isdisjoint(primavera)) # True
print(otonio.isdisjoint(invierno)) # False
