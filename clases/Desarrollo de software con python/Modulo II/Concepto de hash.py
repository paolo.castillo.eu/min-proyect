# hash

# resultado de una función de has
# en la entrada de un dato numerico y devuelve un resultado numerico

# que debe cumplir una función de hash ?
# debe poder calcularse de manera eficiente
# debe entregar resultados dentro de un rango acotado
# debe provocar pocas colisiones

# busuqeda eficiente de elementos
# comparaciones eficientes entre datos
# criptografia y password

# buscando elementos: list
# lista con 100.000 posiciones. queremos saber si 60.5 esta en la lista
# si esta, lo encontraremos, en promedio, despues de recorrer 50.000 posiciones
# si no esta, debemos mirar todas las posciones
# mientras mas posiciones hay, mas nos demoraremos

# buscando elementos: tabla de hash
# estructura para encontrar elementos de manera eficiente
# el hash de cada valor indica su posicion
# si dos valores tienen el mismo hash, se encadenan en una lista

# estrcutura para encontrar elementos de manera eficiente
# el hash de cada valor indica su posición
# si dos valores tienen el mismo hash, se encadenan en una lista


# elementos Hashable

# funcion hash()
# hash para int, float, bool, string, tuple
# el mismo hash para cada valor durante toda la ejecución

from collections import deque


print(hash(10)) # 10
print(hash(60.5)) # 1152921504606847036
print(hash('lisa')) # 7167753761327027290
print(hash((1,2))) # -3550055125485641917
print(hash('lisa')) # 7167753761327027290


# elementos NO Hashable

# no hay hash para list, ni para deque
# tampoco para tuple que contengan elementos no hasheables

# print(hash([1,2]))
# print(hash((1, [2,1], 1)))
# print(hash(deque(1,2)))

