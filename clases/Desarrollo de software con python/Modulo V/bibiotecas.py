import math
# funciones matematica 
# constante como PI
import random
# numeros aleatorios y realizar acciones con ellos.

print(f"Valor de PI es {math.pi}")
print(f"Valor de raiz de 2 es {math.sqrt(2)}")
print(f"El logaritmo base 10 de 256 es {math.log10(256)}")
print(f"El factorial de 10 es {math.factorial(10)}")
print(f"Prodemos aproximar un 3.95 a {math.ceil(3.95)}")
print(f"Las combinaciones del loto son {math.comb(41,6)}")

# importación directa
# puede sobre escribir algunas funciones
#from math import pi, sqrt, log10, factorial, ceil, comb
# print(f"Valor de PI es {pi}")
# print(f"Valor de raiz de 2 es {sqrt(2)}")
# print(f"El logaritmo base 10 de 256 es {log10(256)}")
# print(f"El factorial de 10 es {factorial(10)}")
# print(f"Prodemos aproximar un 3.95 a {ceil(3.95)}")
# print(f"Las combinaciones del loto son {comb(41,6)}")

print(f"un dia cualquiera: {random.randint(1,31)}")
mes = ['ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic']
print(f"un mes cualquiera: {random.choice(mes)}")
print(f"tres meses con repeticion: {random.choices(mes, k=3)}")
print(f"cuatro meses sin repeticion: {random.sample(mes, k=4)}")
print(f"desordenados ...")
random.shuffle(mes)
print(mes)

# from random import randint, choice, choices, sample, shuffle
# print(f"un dia cualquiera: {randint(1,31)}")
# mes = ['ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic']
# print(f"un mes cualquiera: {choice(mes)}")
# print(f"tres meses con repeticion: {choice(mes, k=3)}")
# print(f"cuatro meses sin repeticion: {sample(mes, k=4)}")
# print(f"desordenados ...")
# shuffle(mes)
# print(mes)

# paquetes
# un paquete permite aislar un directorio
# puede incluir otros paquetes y modulos

