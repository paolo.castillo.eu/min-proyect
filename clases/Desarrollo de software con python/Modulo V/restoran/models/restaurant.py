import random

class Restaurant:
    
    def __init__(self, n, d):
        self.nombre = n
        self.direccion = d
        self.platos = []
        self.chefs = []
        self.ingreso = 0
    
    def atender(self, nplatos):
        for k in range(nplatos):
            plato = random.choice(self.platos)
            chef =  random.choice(self.chefs)
            self.ingreso += chef.cocinar(plato)
            print(f"chef {chef.nombre} preparando {plato.nombre}")
    
    def agregar_plato(self, plato):
        self.platos.append(plato)
    
    def agregar_chef(self, chef):
        self.chefs.append(chef)