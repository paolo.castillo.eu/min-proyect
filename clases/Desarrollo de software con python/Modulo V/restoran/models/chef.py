class Chef:

    def __init__(self, n):
        self.nombre = n
        self.cocinados = {} # dict()
    
    def cocinar(self, plato):
        print(f"Chef {self.nombre} cocinando {plato.nombre}")
        if plato.nombre in self.cocinados:
            self.cocinados[plato.nombre] += 1
        else:
            self.cocinados[plato.nombre] = 1
        return plato.precio
