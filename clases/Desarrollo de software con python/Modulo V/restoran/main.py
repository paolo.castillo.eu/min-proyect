from models.plato import Plato
from models.chef import Chef
from models.restaurant import Restaurant

if __name__ == '__main__':
    pica = Restaurant("La pica de python", "VM 4860")
    chef1 = Chef("Cristian")
    chef2 = Chef("Jaime")
    chef3 = Chef("Denis")
    plato1 = Plato("Pizza de funciones", 3000)
    plato2 = Plato("Pastel de modulo", 5000)
    plato3 = Plato("Tuplas a lo pobre", 6000)
    plato4 = Plato("Spaghetti con deque", 4000)
    plato5 = Plato("Reineta al diccionario", 7000)
    pica.agregar_chef(chef1)
    pica.agregar_chef(chef2)
    pica.agregar_chef(chef3)
    pica.agregar_plato(plato1)
    pica.agregar_plato(plato2)
    pica.agregar_plato(plato3)
    pica.agregar_plato(plato4)
    pica.agregar_plato(plato5)
    pica.atender(10)
    print(f"El restaurant ha recuadado {pica.ingreso}")