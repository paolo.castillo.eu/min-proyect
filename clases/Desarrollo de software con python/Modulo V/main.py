# import classpack.Curso as curso
# import classpack.Roles as roles
from classpack.Curso import Curso
from classpack.Roles import Alumno, Profeso

# se debe llamar las funciones con el nombre de la importación.metodo
# crear directorio y colocar todas las clases dentro de ese
# directorio
# crear __init__.py = archivo vacios python interpreta que es un paquete para importar

if __name__ == '__main__':
    profesor = Profeso('Paolo', 28, '09', 'DCC')
    
    al1 = Alumno('Paolo', 20, '123', 2018)
    al2 = Alumno('Rocio', 19, '456', 2019)
    al3 = Alumno('Dario', 23, '789', 2015)
    al4 = Alumno('Rafael', 22, '021', 2018)
    al5 = Alumno('Aurora', 22, '345', 2018)

    curso_dsp = Curso(
        'Desarrollo de software en python', 2021, 1, 10, True)

    curso_dsp.asignar_profesor(profesor)
    curso_dsp.profesor.dictar_clase()
    print(curso_dsp)

    # Curso: Desarrollo de software en python
    # El profesor Paolo esta disctando una clase.



    curso_dsp.inscribir_alumno(al1)
    curso_dsp.inscribir_alumno(al2)
    curso_dsp.inscribir_alumno(al3)
    curso_dsp.inscribir_alumno(al4)
    curso_dsp.inscribir_alumno(al5)

    profesor.evaluar_alumno(al1, 3.5)
    profesor.evaluar_alumno(al2, 6.8)
    profesor.evaluar_alumno(al3, 3.2)
    profesor.evaluar_alumno(al4, 6.4)
    profesor.evaluar_alumno(al4, 7.0)
    profesor.evaluar_alumno(al3, 3.5)
    profesor.evaluar_alumno(al1, 5.5)
    profesor.evaluar_alumno(al2, 3.4)
    profesor.evaluar_alumno(al1, 2.9)
    profesor.evaluar_alumno(al5, 6.2)
    profesor.evaluar_alumno(al5, 5.5)
    profesor.evaluar_alumno(al4, 2.7)

    print(f" Promedio del curso: {curso_dsp.caclular_promedio()}")

    #Promedio del curso: 4.726666666666667

    print(al1)
    print(al2)
    print(al3)
    print(al4)
    print(al5)

    # Estudiante Paolo. promedio: 3.966666666666667. ¿Apruebo? True
    # Estudiante Rocio. promedio: 5.1. ¿Apruebo? True
    # Estudiante Dario. promedio: 3.35. ¿Apruebo? False
    # Estudiante Rafael. promedio: 5.366666666666667. ¿Apruebo? True
    # Estudiante Aurora. promedio: 5.85. ¿Apruebo? True

    # concepto de herencia
    # super clase de persona y alumno => persona
    # ellos heredan de la clase persona

    # curso semanticamente no pertence a persona
    # relación semantica que en globe una clase a la inferiores
