class Curso:
    def __init__(self, n, a, s, c, m) -> None:
        self.nombre = n
        self.anio = a
        self.semestre = s
        self.creditos = c
        self.minimo = m
        self.profesor = None
        self.lista_de_curso = []

    def asignar_profesor(self, p):
        self.profesor = p

    def __str__(self) -> str:
        return f"Curso: {self.nombre}"

    def  inscribir_alumno(self, alumno):
        self.lista_de_curso.append(alumno)

    def caclular_promedio(self):
        suma = 0
        for est in self.lista_de_curso:
            suma += est.calcular_promedio()
        
        return suma / len(self.lista_de_curso)