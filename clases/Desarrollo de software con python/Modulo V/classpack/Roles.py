from classpack.Persona import Persona

# Clase profesor hereda de persona
class Profeso (Persona):

    def __init__(self, n, e, o, d):
        # al iniciar la clase profesor
        # lo delega a la clase superior
        # a la clase persona al metodo inicializador
        # init llama a init de la clase superior
        super().__init__(n, e)
        # self.nombre = n
        # self.edad = e
        self.oficina = o
        self.departamento = d

    def dictar_clase(self):
        print(f"El profesor {self.nombre} esta disctando una clase.")

    # recibe un objeto tipo alumno y nota
    def evaluar_alumno(self, estudiante, nota):
        # lista de notas, se agrega nota en base al objeto alumno
        estudiante.nota.append(nota)

    def __str__(self):
        return f"{self.nombre} del departamento {self.departamento}, oficina: {self.oficina}"

class Alumno (Persona):

    def __init__(self, n, e, num_al, a):
        # init llama a init de la clase superior persona
        super().__init__(n, e)
        # self.nombre = n
        # self.edad = e
        self.numero_alumno = num_al
        self.anio = a
        self.avance = 0
        self.comprension = 0
        self.nota = []

    def aprueba(self):
        return (sum(self.nota) / len(self.nota)) > 3.95

    def estudiar(self, mins):
        self.avance += 0.05 * mins

    # objetos alumnos y cantidad de minuto
    def estudiar_en_grupo(self, alumno, mins):
        # modificar el atributo
        self.comprension += 0.1 * mins
        alumno.comprension += 0.1 * mins

    def calcular_promedio(self):
        return sum(self.nota) / len(self.nota)

    def __str__(self):
        return f"Estudiante {self.nombre}. promedio: {self.calcular_promedio()}. ¿Apruebo? {self.aprueba()}"
