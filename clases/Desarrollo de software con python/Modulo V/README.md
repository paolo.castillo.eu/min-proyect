# Que son los modulos
- código que puedo agregar a mis programas para extender funcionalidades.
- se puede utilizar: [clases, metodos, variables]
- módulos permiten que mi programa pueda utilizar clases o funciones definidas previamente, 
- sin tener que volver a escribirlas
- modulación: cada clases en un unico archivo, ya que se visualiza de forma indipendiente
- un modulo es un troso de codigo ya definiedo que puede agregar a mi programa

`import numpy as np`