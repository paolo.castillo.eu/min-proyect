# construccion de un programa orientado objeto

class Curso:
    
    def __init__(self, n, a, s, c, m) -> None:
        self.nombre = n
        self.anio = a
        self.semestre = s
        self.creditos = c
        self.minimo = m
        self.profesor = None

    def asignar_profesor (self, p):
        self.profesor = p

    def __str__(self) -> str:
        return f"Curso: {self.nombre}"

class Profeso:
    
    def __init__(self, n, e, o, d):
        self.nombre = n
        self.edad = e
        self.oficina = o
        self.departamento = d
    
    def dictar_clase(self):
        print(f"El profesor {self.nombre} esta disctando una clase.")
    
    def __str__(self):
        return f"{self.nombre} del departamento {self.departamento}, oficina: {self.oficina}"
    
if __name__ == '__main__':
    curso_dsp = Curso('Desarrollo de software en python', 2021, 1, 10, True)
    profesor = Profeso('Paolo', 28, '09', 'DCC')
    
    curso_dsp.asignar_profesor(profesor)
    curso_dsp.profesor.dictar_clase()
    print(curso_dsp)
    
    # Curso: Desarrollo de software en python
    # El profesor Paolo esta disctando una clase.