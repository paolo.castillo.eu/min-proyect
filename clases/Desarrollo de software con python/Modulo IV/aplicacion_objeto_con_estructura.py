# construccion de un programa orientado objeto

class Curso:

    def __init__(self, n, a, s, c, m) -> None:
        self.nombre = n
        self.anio = a
        self.semestre = s
        self.creditos = c
        self.minimo = m
        self.profesor = None
        self.lista_de_curso = []

    def asignar_profesor(self, p):
        self.profesor = p

    def __str__(self) -> str:
        return f"Curso: {self.nombre}"

    def  inscribir_alumno(self, alumno):
        self.lista_de_curso.append(alumno)

    def caclular_promedio(self):
        suma = 0
        for est in self.lista_de_curso:
            suma += est.calcular_promedio()
        
        return suma / len(self.lista_de_curso)

class Persona:
    def __init__(self, n, e):
        self.nombre = n
        self.edad = e

# Clase profesor hereda de persona
class Profeso (Persona):

    def __init__(self, n, e, o, d):
        # al iniciar la clase profesor
        # lo delega a la clase superior
        # a la clase persona al metodo inicializador
        # init llama a init de la clase superior
        super().__init__(n, e)
        # self.nombre = n
        # self.edad = e
        self.oficina = o
        self.departamento = d

    def dictar_clase(self):
        print(f"El profesor {self.nombre} esta disctando una clase.")

    # recibe un objeto tipo alumno y nota
    def evaluar_alumno(self, estudiante, nota):
        # lista de notas, se agrega nota en base al objeto alumno
        estudiante.nota.append(nota)

    def __str__(self):
        return f"{self.nombre} del departamento {self.departamento}, oficina: {self.oficina}"

class Alumno (Persona):
    
    def __init__(self, n, e, num_al, a):
        # init llama a init de la clase superior persona
        super().__init__(n, e)
        # self.nombre = n
        # self.edad = e
        self.numero_alumno = num_al
        self.anio = a
        self.avance = 0
        self.comprension = 0
        self.nota = []
    
    def aprueba(self):
        return (sum(self.nota) / len(self.nota)) > 3.95
    
    def  estudiar(self, mins):
        self.avance += 0.05 * mins
    
    # objetos alumnos y cantidad de minuto
    def estudiar_en_grupo(self, alumno, mins):
        # modificar el atributo
        self.comprension += 0.1 * mins
        alumno.comprension += 0.1 * mins
    
    def  calcular_promedio(self):
        return sum(self.nota) / len(self.nota)

    def __str__(self):
        return f"Estudiante {self.nombre}. promedio: {self.calcular_promedio()}. ¿Apruebo? {self.aprueba()}"

if __name__ == '__main__':
    curso_dsp = Curso('Desarrollo de software en python', 2021, 1, 10, True)
    profesor = Profeso('Paolo', 28, '09', 'DCC')

    curso_dsp.asignar_profesor(profesor)
    curso_dsp.profesor.dictar_clase()
    print(curso_dsp)

    # Curso: Desarrollo de software en python
    # El profesor Paolo esta disctando una clase.

    al1 = Alumno('Paolo', 20, '123', 2018)
    al2 = Alumno('Rocio', 19, '456', 2019)
    al3 = Alumno('Dario', 23, '789', 2015)
    al4 = Alumno('Rafael', 22, '021', 2018)
    al5 = Alumno('Aurora', 22, '345', 2018)

    curso_dsp.inscribir_alumno(al1)
    curso_dsp.inscribir_alumno(al2)
    curso_dsp.inscribir_alumno(al3)
    curso_dsp.inscribir_alumno(al4)
    curso_dsp.inscribir_alumno(al5)
    
    profesor.evaluar_alumno(al1, 3.5)
    profesor.evaluar_alumno(al2, 6.8)
    profesor.evaluar_alumno(al3, 3.2)
    profesor.evaluar_alumno(al4, 6.4)
    profesor.evaluar_alumno(al4, 7.0)
    profesor.evaluar_alumno(al3, 3.5)
    profesor.evaluar_alumno(al1, 5.5)
    profesor.evaluar_alumno(al2, 3.4)
    profesor.evaluar_alumno(al1, 2.9)
    profesor.evaluar_alumno(al5, 6.2)
    profesor.evaluar_alumno(al5, 5.5)
    profesor.evaluar_alumno(al4, 2.7)

    print(f" Promedio del curso: {curso_dsp.caclular_promedio()}")
    
    #Promedio del curso: 4.726666666666667
    
    print(al1)
    print(al2)
    print(al3)
    print(al4)
    print(al5)
    
    # Estudiante Paolo. promedio: 3.966666666666667. ¿Apruebo? True
    # Estudiante Rocio. promedio: 5.1. ¿Apruebo? True
    # Estudiante Dario. promedio: 3.35. ¿Apruebo? False
    # Estudiante Rafael. promedio: 5.366666666666667. ¿Apruebo? True
    # Estudiante Aurora. promedio: 5.85. ¿Apruebo? True

    # concepto de herencia
    # super clase de persona y alumno => persona
    # ellos heredan de la clase persona
    
    # curso semanticamente no pertence a persona
    # relación semantica que en globe una clase a la inferiores
