# Tuplas y operaiciones
# es una tructura de datos y acceso eficiente a cualquier posición

tupla = tuple()
print(type(tupla))

profesores = ('jaime', 'cristian', 'denis')
tupla_vacia = ()
mezcla = ('chile', 8, 4.15, 'abril')
sublista = (15, ['arica', 9], 'febrero')
subtupla = (15, ('arica', 9), 'febrero')

print(profesores) # ('jaime', 'cristian', 'denis')
print(tupla_vacia) # () 
print(mezcla) # ('chile', 8, 4.15, 'abril')
print(sublista) # (15, ['arica', 9], 'febrero')
print(subtupla) # (15, ('arica', 9), 'febrero')

# Concadenación

otonio = ['abril', 'mayo', 'junio']
invierno = ['julio', 'agosto']
frio = otonio + invierno
mas_frio = invierno * 2

print(frio) # ('abril', 'mayo', 'junio', 'julio', 'agosto')
print(mas_frio) # ('julio', 'agosto', 'julio', 'agosto')

# consulta

mezcla = ('chile', 8, 4.15, 'abril', 3, 5, '2020', 'MOOC')
# longitud de una lista
largo = len(mezcla)
print(largo) # 8

# elemento en posicion
print(mezcla[0]) # chile
print(mezcla[4]) # 3
print(mezcla[7]) # mooc
print(mezcla[-2]) # 2020

# Extrayendo porciones (slicing)
print(mezcla[2:6]) # (4.15, 'abril', 3, 5)
print(mezcla[:3]) # ('chile', 8, 4.15)
print(mezcla[5:]) # (5, '2020', 'mooc')
print(mezcla[1:6:2]) # (8, 'abril', 5)

# modificar elementos

mezcla = ('chile', 8, 4.15, 'abril', 3, 5, '2020', 'MOOC')
#mezcla[3] = 'mayo'
#print(mezcla) # typeError: 'tuple' object does not support item assignment

 # diferencia entre tuple y list
 # tuplas son inmutables, listas son mutables
 # no podemos modificar elementos ni porciones de una tupla
 # al crear una tupla, sus contenidos permanecen iguales

# Recorrer con for in range:
for i in range(len(mezcla)):
    print(mezcla[i]) # ...

# Recorrer con for in tupla:
for elemento in mezcla:
    print(elemento) # ...

# Encontrar la posición
opciones = ('si','no', 'quizas', 'no se')
print(opciones.index('quizas')) # 2
print(opciones)
