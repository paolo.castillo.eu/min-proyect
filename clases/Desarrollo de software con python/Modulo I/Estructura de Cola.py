# Estructura de Cola
# estructura ordenada

from collections import deque
cola = deque()
print(type(cola))  # <class 'collections.deque'>

profesores = deque(['jaime', 'cristian', 'denis'])
cola_vacia = deque([])
mezcla = deque(('chile', 8, 4.15, 'abril'))
sublista = deque([15, ['arica',9], 'febrero'])

print(profesores) # deque(['jaime', 'cristian', 'denis'])
print(cola_vacia) # deque([])
print(mezcla) # deque(['chile', 8, 4.15, 'abril'])
print(sublista) # deque([15, ['arica', 9], 'febrero'])

# concadenar

otonio = deque(['abril', 'mayo', 'junio'])
invierno = deque(['julio', 'agosto'])
frio = otonio + invierno
mas_frio = invierno * 2

print(frio) # deque(['abril', 'mayo', 'junio', 'julio', 'agosto'])
print(mas_frio) # deque(['julio', 'agosto', 'julio', 'agosto'])

# consulta sobre una cola
mezcla = deque(['chile', 8, 4.15, 'abril', 3, 5, '2020', 'MOOC'])

# longitud de una cola
largo = len(mezcla)
print(largo)  # 8

# elemento en posicion
print(mezcla[0]) # chile
print(mezcla[4]) # 3
print(mezcla[7]) # mooc
print(mezcla[-2]) # 2020

# Extrayendo porciones (slicing)
# print(mezcla[2:6])  # Error 
# print(mezcla[:3])  # Error
# print(mezcla[5:])  # Error
# print(mezcla[1:6:2])  # Error


# Modificar elementos de unua cola
mezcla = deque(['chile', 8, 4.15, 'abril', 3, 5, '2020', 'MOOC'])

mezcla[3] = 'Mayo'
mezcla[4] += 5
print(mezcla)  # deque(['chile', 8, 4.15, 'Mayo', 8, 5, '2020', 'MOOC'])

# recorriendo cola
# recorriendo cola con for in range:
for i in range(len(mezcla)):
    print(mezcla[i])  # ...

# Recorrer con for in cola:
for elemento in mezcla:
    print(elemento)  # ...

# Operaciones sobre colas: agregar y sacar desde el final

# Agregar al final: append
opciones = deque(['si', 'no', 'quizas'])
opciones.append('no se')
print(opciones)  # deque(['si', 'no', 'quizas', 'no se'])

# sacar desde el final : pop
cuando = deque(['mañana','pasado','hoy'])
eliminado = cuando.pop()
print(cuando)  # deque(['mañana', 'pasado'])
print(eliminado)  # hoy

# agregar al inicio: appendleft
opciones = deque(['si', 'no'])
opciones.appendleft('no se')
print(opciones) # deque(['no se', 'si', 'no'])

# sacar al inicio: popleft
cuando = deque(['mañana', 'pasado', 'hoy'])
eliminado = cuando.popleft()
print(cuando)  # deque(['pasado', 'hoy'])
print(eliminado)  # mañana

# operación para insertar un elemento
opciones = deque(['si', 'no', 'quizas', 'no se'])
opciones.insert(2, 'quizas')
print(opciones) # deque(['si', 'no', 'quizas', 'quizas', 'no se'])

# encontrar posición de elemento index
opciones = deque(['si', 'no', 'quizas', 'no se'])
print(opciones.insert(2,'quizas'))
print(opciones)  # deque(['si', 'no', 'quizas', 'quizas', 'no se'])

# eliminar elemento remove
cuando = deque(['mañana', 'pasado', 'hoy'])
cuando.remove('pasado')
print(cuando)  # deque(['mañana', 'hoy'])
