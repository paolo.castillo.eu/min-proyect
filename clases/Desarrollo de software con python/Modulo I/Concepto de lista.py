# Concepto de lista 
# Colección o agrupación de datos
list = list()
print(type(list))

teachers = ['','','']
student = ['','']
new_list = teachers + student

# longitud de una lista
largo = len(teachers)
print(largo)

# Elemento en posicion
print(teachers[1])

# Extrayendo porciones (slicing)

print(teachers[2:6])
print(teachers[:3])
print(teachers[5:])
# de la posicion 1 al 6 en salto de dos
print(teachers[1:6:2])

# Modificación de elementos por posicion
mezcla =  ['chile', 8, 4, 15, 'Abril', 3, '5', '2020', 'MOOC']
mezcla[3] = 'Mayo'
mezcla[4] +=5
print(mezcla)

# modificar rangos de elementos
mezcla[2:5] = [-1,0,-2]
print(mezcla) # ['chile', 8, -1, 0, -2, 5, '2020', 'MOOC']

mezcla[1:6] = ['x']
print(mezcla) # ['chile', 'x', '2020', 'MOOC']

# Recorriendo por un for _in rango:
for i in range(len(mezcla)):
    print(mezcla[i])
    
# Recorriendo con for _ in list:
for elemento in mezcla:
    print(elemento)

# Operaciones de lista

# Agregar al final: append
opciones = ['si','no']
opciones.append('no se')
print(opciones) # ['si','nose']

# Eliminar desde el final pop
cuando = ['mañana','pasado','hoy']
eliminado = cuando.pop()
print(cuando) # ['mañana','pasado']
print(eliminado) # hoy

# insertar en posicion insert
opciones = ['si','no','no se']
opciones.insert(2,'quiza')
print(opciones) # ['si','no', 'quizas', 'no se']

# Eliminar elmento pop
cuando = ['mañana','pasado','hoy']
eliminado = cuando.pop(1)
print(cuando) # ['mañana','hoy']
print(eliminado) # pasado

# Encontrar posicion de elemento index
opciones = ['si','no', 'quizas', 'no se']
print(opciones.index('quizas'))
print(opciones) # 2

# eliminar elemento remove
cuando = ['mañana','pasado','hoy']
cuando.remove('pasado')
print(cuando) # ['mañana', 'hoy']
