
class Auto:  # clases

    # difinicion de atributos
    # self = auto-referencia de referencias
    # se usa en los metodos para referirse al atributo
    # o metodo del objeto actual sobre el que estamos trabajando
    # self es una convencion. puede ser cualquier nombre

    # Inicializador
    def __init__(self, ma, mo, a, c, k):
        self.marca = ma
        self.modelo = mo
        self.anio = a
        self.color = c
        self.__kilometraje = k
        self.__ubicacion = (-33.45, -70.63)
        self.duenio = None
    
    # representación de un objeto en string
    def __str__(self):
        # no usa print
        # solo return
        return f'Auto de {self.duenio}. {self.modelo}, año {self.anio}, kms {self.__kilometraje}'

    # metodo para conducir
    def conducir(self, kms):
        self.__kilometraje += kms
        self.__modificar_ubicacion(kms/20, kms/30)

    # vender
    def vender(self, nuevo_duenio):
        self.duenio = nuevo_duenio
    
    # metodo getter private atributo
    def leer_anometro (self):
        return self.__kilometraje

    # metodo oculto
    def __modificar_ubicacion(self, lat, lon):
        print("Calcular nueva ubicación")
        self.__ubicacion = (self.__ubicacion[0]+lat, self.__ubicacion[1]+lon)


class Persona:
    
    def __init__(self, a, b, c, d):
        self.nombre = a
        self.apellido = b
        self.rut = c
        self.edad = d
    
    def __str__(self):
        return f'Soy {self.nombre} {self.apellido} y tengo {self.edad} año'
    
    def cumpleanio(self):
        self.edad += 1
    
    def  deme_su_rut(self):
        return self.rut

def cambiar(persona_1, persona_2):
    nombre = persona_1.nombre
    persona_1.nombre = persona_2.nombre
    persona_2.nombre = nombre

if __name__ == '__main__':
    # creación de instancias de autos (objetos de clase auto)
    a = Auto('kia', 'sportage', 2000, 'blanco', 145230)
    b = Auto('suzuki', 'grand nomade', 2015, 'naranja', 35695)

    # Auto de None. sportage, año 2000, kms 145230
    # Auto de None. grand nomade, año 2015, kms 35695
    print(a)
    print(b)

    a.vender('paolo')
    b.vender('felipe')
    
    # Auto de paolo. sportage, año 2000, kms 145230
    # Auto de felipe. grand nomade, año 2015, kms 35695
    print(a)
    print(b)

    # Calcular nueva ubicación
    # Calcular nueva ubicación
    a.conducir(56)
    b.conducir(1000)
    
    # Auto de felipe. grand nomade, año 2021, kms 36695
    b.anio = 2021
    print(b)

    # no se modifica clase privada
    b.__kilometraje = 500
    print(b)
    
    # Calcular nueva ubicación
    # Auto de felipe. grand nomade, año 2021, kms 37695
    b.conducir(1000)
    print(b)

    c = Persona('Juan', 'Castillo', '19546919-0', 27)
    d = Persona('Diego', 'torres', '18526919-0', 36)

    cambiar(c, d)
    print('-----')

    # Soy Paolo Castillo y tengo 27 año
    # Soy felipe torres y tengo 36 año
    print(c)
    print(d)
    
    print('-----')
    
    c.cumpleanio()
    
    # Soy Paolo Castillo y tengo 28 año
    print(c)
    
    # 18526919-0
    print(d.deme_su_rut())

