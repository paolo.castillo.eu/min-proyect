# Programación orientado objetos
# clase y objetos

# ¿ que es la programación orientada a objetos
# son caracteristicas tangible que puede tener distintos tipo

# auto => marca, modelo, año, color, kilometraje, ubicación, dueño
# conducir
# vender
# pintar

# clases: plantilla de que elementos componen un objeto
# atributos: caracteristicas especifica de cada objetos
# metodos: acciones que se puede aplicar sobre un objeto


class Auto: # clases
    
    # difinicion de atributos
    # self = auto-referencia de referencias
    # se usa en los metodos para referirse al atributo 
    # o metodo del objeto actual sobre el que estamos trabajando
    # self es una convencion. puede ser cualquier nombre
    
    def __init__(self, ma, mo, a, c, k):
        self.marca = ma
        self.modelo = mo
        self.anio = a
        self.color = c
        self.kilometraje = k
        self.ubicacion = (-33.45, -70.63)
        self.duenio = None
    
    # metodos
    def conducir(self, kms):
        self.kilometraje += kms
    
    def vender(self, nuevo_duenio):
        self.duenio = nuevo_duenio
    
    def leer_odometro(self):
        return self.kilometraje

# creación de instancias de autos (objetos de clase auto)
a = Auto('kia', 'sportage', 2000, 'blanco', 145230)
b = Auto('suzuki', 'grand nomade', 2015, 'naranja', 35695)

# invocación de metodos
b.conducir(1450)
a.vender('enrique')
print(b.leer_odometro()) # 37145

print(a.marca) # kia
print(b.color) # naranja
print(a.anio) # 2000
