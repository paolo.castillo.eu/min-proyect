# concepto de atributos

# ¿ que tipo de atributos podemos usar ?
# campo o caracteristicas del objetos
# todos los objetos pertenecientes a la misma clase
# poseen del nombre dele atributo

# ¿ como inicializar ?

class Auto:  # clases

    # difinicion de atributos
    # self = auto-referencia de referencias
    # se usa en los metodos para referirse al atributo
    # o metodo del objeto actual sobre el que estamos trabajando
    # self es una convencion. puede ser cualquier nombre

    # __init__ = inicializador de clas
    def __init__(self, ma, mo, a, c, k):
        self.marca = ma
        self.modelo = mo
        self.anio = a
        self.color = c
        # encapsulamiento
        # objetos que solo deben ser manipulados mediante su conjunto
        # de metodos: su interfaz
        # se empiezan con __no puede ser llamados desde afuera de la
        # class
        # atributos privados
        self.__kilometraje = k
        self.__ubicacion = (-33.45, -70.63)
        self.duenio = None

    # metodos
    def conducir(self, kms):
        self.__kilometraje += kms
        self.__modificar_ubicacion()

    def vender(self, nuevo_duenio):
        self.duenio = nuevo_duenio

    def leer_odometro(self):
        return self.__kilometraje

    def __modificar_ubicacion(self):
        print('calcular nueva ubicación')
        self.__ubicacion = ...

# creación de instancias de autos (objetos de clase auto)
a = Auto('kia', 'sportage', 2000, 'blanco', 145230)
b = Auto('suzuki', 'grand nomade', 2015, 'naranja', 35695)

print(a.modelo) # lectura del atributo de a
a.color = 'azul'
print(a.color) # azul
# print(a.__kilometraje) # provoca error
# a.__kilometraje += 42  # provoca error
a.conducir(42)

print(a.leer_odometro())