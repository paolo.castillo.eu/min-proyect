# K-Means

- Introducción
- Algoritmo
- Ejemplo

## Introducción

### ¿Qué es K-means?

clustering k-means es un metodo de cuantificación vectorial originalmente a partir del procesamiento de señales con el objetivo de dividir en *n* observaciones en *k* agrupaciones de cada observaciones que pertenece al agrupamiento con la media más cercana que es el "Centro de agrupación o centroide de agrupación".

### Objetivo

El objetivo que persigue este método consiste encontrar grupos de datos similares o clusteres.

![alt text](k-means-objectivo.png)

## Algoritmo

### Valor de K

Se debe definir el valor de K o numero de clusters inicialmente este valor es un valor del algoritmo que se debe definir al comienzo y antes de ejecutar el algoritmo. No existe un valor definido para K.

![alt text](k-means-1.png)

Una vez definido en valor de K se generan los K centros aleatoriamente.

![alt text](k-means-2.png)

Seguidamente se asigna de cada punto del set de datos al K más cercano del punto.

![alt text](k-means-3.png)

### Centroides o centro de clusters

Cada vez que se actutaliza los centros, cada cluster tiene una nueva posición.
Este proceso es iterativo, se detiene al converger a los mejores centros.

![alt text](k-means-centroides.png)

### Criterio de detención

Este proceso iterativo, finaliza en cuando la posición de los clusters no sufre cambios significativos. Es posible definir, un periodo de detención de este algoritmo por ejemplo definir un delta (distancia muy pequeña) que se usará para determinar si la posición de los centros es mayor o menor de este delta con la finalidad de deterner el clico iteractivo.

## Ejemplo

Propiedad-ingreso

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| Antonio    |  10          |       260        | 89                 |
| Magdalena  |  3           |       350        | 190                |
| Javiera    |  4           |       103        | 170                |
| Nicolás    |  12          |       390        | 75                 |
| Agustín    |  1           |       900        | 200                |    
| Florencia  |  5           |       230        | 79                 |

1. Normalización de los datos, atravez de la función min-max para que esten más homogéneos en un rango entre 0 y 1.

2. Selección aleatoria de cluster iniciales

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| cluster1   |  0.42        |       0.38        | 0.64              | C1
| cluster2   |  0.12        |       0.57        | 0.23              | C2

3. Calcular la distancia hacia los centros de cada clusters (formula euclidiana)

![alt text](clustering.paso-3.png)

4. Se asigna a los clusters correspondiente de acuerdo al valor de menor distancia entre registro y los centros que se este considerando. /(mas cerca al 1) (1 iteración)

![alt text](clustering.paso-4.png)

5. Resultado parcial

| C1         | C2           |
|------------|--------------|
| Antonio    |  Florencia   |
| Magdalena  |              |
| Javiera    |              |
| Nicolás    |              |
| Agustín    |              |

6. Actualizar los centros 

C1

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| Antonio    |  0.82        |       0.20        | 0.11                |
| Magdalena  |  0.18        |       0.31        | 0.92                |
| Javiera    |  0.27        |       0.00        | 0.76                |
| Nicolás    |  1.00        |       0.36        | 0.00                |
| Agustín    |  0.00        |       1.00        | 1.00                |    

C2

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| Florencia  |  0.45        |       0.37        | 0.56              |

7. Calcular hacia los nuevos centros (2 iteración)

![alt text](clustering.paso-7.png)

![alt text](clustering.paso-7.5.png)

8. Resultado partical de la iteración 2

| C1         | C2           |
|------------|--------------|
|  Magdalena |  Antonio     |
|  Javiera   |  Nicolás     |
|  Agustín   |              |

9. Actualizar los centros en base al iteracion 2

C1

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| Magdalena  |  0.18        |       0.31        | 0.92              |
| Javiera    |  0.27        |       0.00        | 0.76              |
| Agustín    |  0.00        |       1.00        | 1.00              |    

C2

| Nombre     | Propiedades  | Ingreso Mensual   | Pago Impuesto     |
|------------|--------------|-------------------|-------------------|
| Nicolás    |  1.00        |       0.36        | 0.00              |
| Antonio    |  0.82        |       0.20        | 0.11              |

10. Cálcuilar hacia los nuevos centros (3 iteración)

![alt text](clustering.paso-10.png)

![alt text](clustering.paso-10-5.png)

11. Detención del algoritmo

![alt text](clustering.paso-11.png)

# Sintesis

- Técnica de agrupamiento de datos a través de calculo de distancias.
- Ampliamente usado para análisis de datos a través de clusters.
- Fácil de entender y rápido procesamiento de datos.
