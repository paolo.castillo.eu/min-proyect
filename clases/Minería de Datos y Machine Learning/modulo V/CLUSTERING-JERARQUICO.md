# Clustering jerárquico

- Introducción
- Clustering Jerárquico

## Introducción

### ¿ Qué es clustering jerárquico aglomerativo ?

Es un metodo bastante sencillo y útil en la práctica. La idea principal del clustering jerárquico es que a partir de una medida de similaridad, los puntos más cercanos dentro de los datos se junta paso a paso, generando una jerarquía de resultado de clusteing.

![alt text](clustering-jerarquico.png)

## Clustering Jerárquico

### Pasos

Los pasos son bastante sencillos, en cada iteración, el par de cluster más cercano se van juntando. Considerando que cada punto del set de datos es un clister po si solo.
De esta manera los 2 punto más cercanos forman un cluster llamado 1.

![alt text](clustering-jerarquico.png)clustering-jerarquico1.png

A continuación se realiza los siguientes iteracciones juntándose los 2 clusters más cercano.

![alt text](clustering-jerarquico.png)clustering-jerarquico2.png

Se debe continuar con la agrupamiento de los mar cercanos como se observa en la primera central denomiando 3 toma los cluster que están mas hacia a la izquierda y en la imagen a la derecha denomiando 4 toma el cluster 2 agrupando un nuevo elemento conformando el cluster numero 4.

![alt text](clustering-jerarquico3.png)

Finalmente llegara a un solo cluster que contendra todos los clusters que son registro. Siempre es posible detener el algoritmo antes de llegar a crear un gran clusters, en este caso se debe contar con un criterio.

![alt text](clustering-jerarquico4.png)

### Criterios de detención

Entre los criterios de detención del algoritmo se encuentra:

- Número minímo de clusters
- Umbral de distancia máxima
- Número máximo de iteraciones

Estos criterios se aplican de manera diferenta en diversas base de datos. Por lo que requiere un entendimiento del set de datos antes de ser aplicado.

### Medición de distancia entre clusters

Ademas se debe realizar la distancia entre clusters con algunas tecnicas:

- Conexión simple
- Conexión completa
- Distancia entre medias
- Distancia promedio entre pares

#### Conexión simple

Se calcula la mínima distancia entre todos los pares del punto de ambos clusters y se selecciona la menor.

![alt text](clustering-jerarquico-conexion-simple.png)

#### Conexión completa

Se define en lo opuesto de la conexión simple (maxima distancia)

![alt text](clustering-jerarquico-conexion-completa.png)

#### Distancia entre medias

Se definie aquella distancia que existe enre los centroides de cada grupo

![alt text](clustering-jerarquico-conexion-distancia-medidas.png)

#### Distancia promedio entre pares

Es la medida que corresponde al promedio de todos los puntos de distancias.

![alt text](clustering-jerarquico-conexion-distancia-promedio.png)

# Síntesis

- Este método busca crear jerarquías de grupos.
- No se necesita determinar previamente el número de clusters.
- No recomendable para set de datos muy grandes.

