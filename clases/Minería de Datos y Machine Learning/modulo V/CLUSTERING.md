# Clustering

- Introducción

## ¿Que es clustering?

Su tarea principal de agrupar un con junto de objetos de tal manera que los objetos del mismo grupo o clustergin esten agrupado entre si con los otros grupo. Es una tarea principal de la mineria de datos explotaroria y una tecnica comun para el analisis estadistico de datos utilizada en muchos campos ej:

- reconocimiento de patrones
- analisis de imagenes
- recuperación de información
- bioinformatica
- compresion de datos
- graficos por computadora
- aprendizaje automatico

## Caracteristicas

Se desea encontrar similitudes entres los datos de acuerdo con las caracteristicas encontradas en sus atributos. Es un tipo de aprendizaje no supervisado, ya que no hay clases prefeinidas. Entre las medidas de distancia mas utilizadas, se puede encontrar:

- Euclidiana
- Hamming
- Jaccard
- Coseno
- Edit

## ¿Por qué son necesarios los clusters?

Son necesario, ya que un conjunto de datos que no se sabe nada. Un algoritmo de clustering púede descubrir grupos de objetos de distancia promedio de cada grupo estan más cerca de otros grupo para realizar inferencia.

![alt text](clustering.png)

## Existe diversos tipo de clustering

### K-Cento
Uno de los utilizados es el basado en centro, es el mas comun y agrupa las observaciones en 3 clusters distintos, donde el numero K se determina previamente antes de ejecutar el algoritmo. 
Uno de los mas comunes busca los k-centos des clusters asignado los objetos al centro del clusters mas cercano reduciendo la distancia al minimo.

![alt text](clustering-cento.png)

### Clustering basado en distribución

Otra forma de clustering es el basado en distribución, donde se considera que los datos de una distribución es una mescla de uno o mas clusters a diferencia de K-means, el basado en distribución utiliza una asignación flexible donde cada punto de datos tiene una probabilidad de pertenecer a cada clusters

![alt text](clustering-basado.png)

### clustering basado en densidad

Se definen como áreas de mayor poblamiento de datos que el resto de conjunto de datos.Los objetos de zona poco poblado que se requiere en grupo separados, estan generalmente considerado como ruido o puntos fronterizos. El metodo mas popular de la densidad basada en la agrupación es dbsscam: Esta tecnica se basa en conectar los puntos en ciertos lumbrales de distancias 

![alt text](clustering-basado-densidad.png)

## Ejemplos de usabilidad

- Biologia: categorizar genes
- Análisis de redes sociales
- Segmentación de cliente
- Complementariedad con otras tecnicas de mineria de datos.

## Sintesis

- Clustering permite analizar datos que tenga cierta similitud
- Utiliza métrica de distancia para agrupar datos.
- Método de aprendizaje no supervisado.
- Método de aprendizaje no supervisado.
- Técnica común para el análisis de datos estadisticos utilizado en muchos campos.
