# Clase I

## Datos

RAE: Algo concreto que permite su conocimiento exacto o sirve para deducir las consecuencias derivadas de un hecho. A este problema le faltan datos numéricos.

Tipos y ejemplos de datos
- Tipo de datos primitivos
    - Numericos
    - Texto
    - Boleanos

- Tipo de datos compuesto
    - Vectores
    - Conjuntos
    - Tuplas

## Ejemplo
```
Nombre: Javier Benitez
Ocupación: Ingeniero
Género: Masculino
Edad: 41
Altura: 1.76
Grados Ac: [Dr. Msc]
```

## ¿Por qué son importantes los datos?

- Usar los datos para hacer análisis descriptivos y descubrir tendencias.
- Los datos adecuados pueden ayudar a obtener conocimiento útiles para tomar dicisiones mucho mejores.
- La aceptación de los datos y las decisiones basadas en la evidencia dan una ventaje competitiva a los ejecutivos de ventas.
- Hacer el análisis predictivo para ver qué le dicen los datos sobre lo que va a ocurrir.
- Use el análisis preceptivo para deteminar lo que debe hacer a continuación.
- Pëse al avance en el análisis de datos, casi todos los gerentes se basan en su instinto para tomar decisiones críticas.

## Un mundo de posibilidades a través de la toma de decisiones

- Medicina
- transporte
- Agricultura
- Banca
- Markting y comercio
- Clasificación de imágenes

## Inteligencia de Negocios v/s Machine Learning

- Inteligencia de Negocios: Tendencia de grupo Dashboards
    - Qlik
    - Oracle
    - Tableau
    - Oracle
    - Power BI
    - SAP/BI

- Machine Learning: Predicciones individuales, aplicaciones predictivas.

## Metodología KDD (Knowledge Discovery in Databases)

- Seleccionar datos de una DB
- Procesamiento de datos - prec procesados
- Transformación de los datos
- Aplicar modelo o patrones
- Conocimiento de evaluación

## Metodología CRISP-DM (Cross-Industry Standard Process for Data Mining)

- Necesidad de cliente
- Estudio y compresión de datos
- Análisis de los datos y selección de características
- Modelado
- Evalución (obtención de resultado)
- Despliegue (puesta en producción)

## Machine Learning

- Aprendizaje Supervisado
    - Árboles de decisión (conceptración de ganancía)
    - Regresión de mínimos cuadrados (proyección)
    - Regresión Logísticas (0,1 aproximación) tomar una curva y cual decición debo tomar.
    - Clasificadores (KNN)
- Aprendizaje No Supervisados
    - Clustering : medición de distancia
    - Reglas de asociación

## Herramienta
- Anaconda
- Google Colab
