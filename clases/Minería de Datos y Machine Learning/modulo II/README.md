# ¿Por que preprocesar los datos?
- Datos incompletos: falta de valores en algunos atributos
- Datos ruidosos: ingreso de los sistema de información 
- Datos inconsistente: relación en nombre de atributo conocidad por diversas áreas
- Muchos datos: indica es necesario reducir la información, para tener mejor análisis.

## Análisis descriptivos de los datos
- Visión: algunas características generales de los datos.
- Útil: tener una primera mirada sobre los datos.
- Uso: medida de tendencia central. Medidas dispersión de datos (variación de la variable, dispersión)

## Medidas de tendencia central

- Medida aritmética (suma total de dato / total de datos)

![alt text](medida-aritmetica.png)

- Media aritmetica con peso (importancia relativa (dato*valor/suma de valores))

![alt text](medida-aritmetica-peso.png)

- Media central (ordena los datos y se excluyen los datos de los extremos)

![alt text](medida-tendencia-central.png)

- Mediana (valor que ocupa en su puntación central)

![alt text](mediana.png)

- Moda (es el dato que mas veces se repite, frecuencia absoluta)

![alt text](moda.png)

- Medidas distribuidas (sub-dividir el set de datos para obtener una sub-medida, en cada uno de ellos se obtiene el maximo)

![alt text](moda-distribuidas.png)

### Medida aritmética

### Medida de dispersión de datos
- Rango (diferencia entre el menor de la variable y mayor)

![alt text](rango.png)


- Desviación estándar (cuantificar la dispersión de un conjunto de datos numericos, una desviación estandar baja estar agrupado a su media, pero una alta se extendiente sobre sus valores mas amplio)

![alt text](desviacion-estandar.png)

- K-ésimo percentil (los cuartiles deviden un conjunto de datos, en 4 parte iguales y sus punto de división son los percetiles 25, 50, 75 y se puede encontrar su maximo, los punto alejados son outliers-son incorrecto-viene de una población distintas)

![alt text](k-esimo-percentil.png)


# ¿Por qué limpiar los datos?

- Datos en el mundo real
    - incompletos
    - ruidosos
    - inconsistentes
- Proceso de limpieza de datos
    - Llenar los valores que faltan.
    - Identifica valores erróneos corrigiéndolos.
    - Elimina inconsistencias en la información.

- Datos Faltanes
    - Ignorar la tupla.
    - Llenar los valores manualmente.
    - Usar una constante para llenar los valores.
    - Usar la media del atributo.
    - Usar el valor más probable.

- Algunas técnicas para procesar datos.
    - Binning 
        - Agrupación de datos, de procesamiento de datos para reducir algunos errores (intervalos pequeños).
        Ej: 
            Smoothing by bin means: cada valor de intervalor se remplazo por su mediana
            Smoothing by bin boundaries: los valores minimo y maximo de un bin de intervalor se determional y luego de remplza con su valor mas cercano.
    - Regresión
        - Consiste en estimar los valores ausentes, en base a los valores validos de otros valores. Consiste en estimar su valores acorde a su estimación.
    - Clustering
        - Consiste en agrupar registro acuerdo a una similitud para completar los datos faltante, en lo cual puede estimar datos outliers

- Consideracones en la integración de datos.
    - Algunos problemas tipicos
        - Identificanción de la identidad.
            - misma entidad
                - Distintos nombre en diferentes fuentes de datos
                Ej: customer_id, cust_number
        - Redundancia.
            - un atributo redundante si puede ser derivado de otro.
                - Errores en la identificación de la entidad suelen llevar a situaciones de redundancias.
        - Detención y resolución de conflictos entre valores.
            - misma entidad
                - valores del atributo proveniente de distintas fuentes de datos es diferentes.
                - puede tener diferente presentación.

- Transformación de datos
    - Datos: Se transforman y consolidan de modo que este listo para el proceso de mineria de datos.
    - Objetivo: Lograr que los datos medidos en escalas diferentes ean más comparables entre si.
    - Permite: mejorar las supociones de algunas técnicas estadisticas.

## ¿Como transformar datos?

- técnicas:
    - Smoothing.
        - Binning.
        - Regresión.
        - clustering.
    - Normalización.
        - cambiar los valores numerica a una escala común.
        - KNN, clustering, etc.
        - Normalización de min-max.
        - Normalización z.score.
            - los valores para un atributo A son normalizados en base a la media y la desviación estándar.
            ![alt text](normalizacion-score.png)
        - Normalización decimal
            - los valores para un atributo A son normalizados moviendo los punto decimal.
            ![alt text](normalización-decimal.png)
    - Agregación
        - Estrategias:
            - Agregación en el cubo de datos.
                - Algunos datos son agregados dependiendo de la información que se desea manejar.
                - Se logra una importante reducción de la cantidad de información sin pérdida de datos necesario para el análisis.
                EJ: solo se desean las ventas anuales, por lo tanto, se deben sumar los datos mensuales.
            - Reducción de dimensionalidad.
                - Aplicación de transformación de los datos para obtener una versión comprimida de ellos.
                - Versión comprimida tiene bajos porcentajes de pérdida de información.
                - Técnica comúnmente utilizada:
                    Pricipal Components Analysis (PCA)
            - Seleccion de un subconjunto de atributos.
                - Existen atributos en la base de datos que para ciertos análisis son irrelevantes.
                - Los patrones descubiertos sobre una cantidad reducida de atributos más entendibles por el usuario.
                - Algoritmo eficientes, para n atriubtos existen2 posibles subconjuntos de atributos
                - Algoritmo conocidos: Stepwise forward selection, stepwise backward elimination, Plus R take away R, Exhaustive search, etc.
            - Discretización (bining, generación de rangos).
            - Jerarquías de conceptos (generealización).
    - Generalización