# Instroducción

- Clasificación automatica
- Ejemplo
- Construcción árbol de decisión
- Entropía
- Ganancia de información

## Clasificación automatica

Un problema de clasificación automatica busca encontrar un *sistema* capaz de identificar para cada objeto la clase a la cual pertenece.

1. Técnica de clasificación.
2. Nodos internos del árbol representante de un atributo.
3. Cada nodo realiza un test basado en los valores del atributo al cual representa.
4. Técnica de aprendizaje supervisado
5. Un set de registros pre-clasificado: *set de entrenamiento*.

## Ejemplo

- Aprobar o Rechazar postulación

 
| Postulante | Promedio | Test Adminisión   | Postulación   |
|------------|----------|-------------------|---------------|
|    M       |  Bajo    |       Bueno       | Aceptado      |
|    H       |  Alto    |       Malo        | Rechazado     |
|    H       |  Bajo    |       Bueno       | Aceptado      |
|    M       |  Alto    |       Bueno       | Aceptado      |
|    H       |  Alto    |       Bueno       | Rechazado     |
|    M       |  Bajo    |       Malo        | Rechazado     |

![alt text](ejemplo-arbo.png)

![alt text](ejemplo-arbo-definicion.png)

![alt text](ejemplo-arbo-clasifiación.png)

### ¿Cuál es la clasificación para postulación de un estudiante con buen promedio y test de adminisión alto?

Para clasificar un registro se debe recorrer el árbol de nodo raiz al nodo hoja que tenga resultado.

![alt text](ejemplo-arbo-dicision.png)

## Construcción árbol de decisión

1. Elejir la mejor variable : Postulante del set de datos

| Postulante | Promedio | Test Adminisión   | Postulación   |
|------------|----------|-------------------|---------------|
|    M       |  Bajo    |       Bueno       | Aceptado      |
|    H       |  Alto    |       Malo        | Rechazado     |
|    H       |  Bajo    |       Bueno       | Aceptado      |
|    M       |  Alto    |       Bueno       | Aceptado      |
|    H       |  Alto    |       Bueno       | Rechazado     |
|    M       |  Bajo    |       Malo        | Rechazado     |

2. Colocar la variable seleccionada como nodo raíz y poner como indice los valores de dicha columna.

![alt text](construccion-colorcar-variable.png)

3. Separar las filas de las variables agrupando según su valor: hombre y mujer

![alt text](construccion-separar-filas.png)

4. Separar las filas de las base de datos generando sub-set de datos.

![alt text](construccion-seprar-filas-de-la-db.png)

![alt text](construccion-db.png)

56. Separar cada sub set de datos usando otro atributo: Ejemplo, elegir la mejor variable para la base de datos de la izquierda

![alt text](construccion-elegir.png)

78. Elegir la mejor variable de la derecha y seleccionar la mejor variable como nodo.

![alt text](construccion-selecciona-derecha.png)

### Criterios de detención

Al estar en un set de datos, uno se debe preguntar ¿pertenecen todos los registros a la misma clase?, en el caso que sea correcto se debe tomar un nodo hoja con clase respectiva.

![alt text](criterio-detencion-1.png)

Tienen todos los registro el mismo valor para todos atributos que determine su clase, en caso de afirmación se retorna la clase que tiene mas en común.

![alt text](criterio-detencion-2.png)


## Entropía

Es un indicador que nos permite medir el grado de desorden en un conjunto de datos. La formula de entropía se calcula para cada clase:

entropia.png

| Postulante | Promedio | Postulación   |
|------------|----------|---------------|
|    M       |  Bajo    | Aceptado      |
|    H       |  Alto    | Rechazado     |
|    H       |  Bajo    | Aceptado      |

Como elegir en mejor atributo usando la entropia
1. Cacular la entropía del set de datos (Aceptado y rechazado)
2. Dos clase Aceptado y Rechazado

![alt text](entropia2.png)

### Ganancia de información

Ademas de entropía, es importante aplicar otro indicador que permita medir la calidad de una variable.

La ganancia es la diferencia de entropía entre un nivel del árbol y otro al separar con un cierto atributo para estimar ponderaciones de un set de datos.

![alt text](entropia3.png)

### Ejemplo

![alt text](entropia-ganancia.png)

![alt text](entropia-ganancia-postulante.png)

![alt text](entropia-ganancia-postulante-detencion.png)

![alt text](entropia-ganancia-postulante-detencion-final.png)

# Síntesis

- Los árboles de decisión son un método efectivo para la toma de decisiones
- Facilita la interpretación de la decisión adoptada
- Los ámbitos de uso son diverso como por ejemplo para la generación de sistemas expertos, búsquedas binarias y árboles de juegos.
- Para la construcción del árbol de decisión es importante considerar la entropía y la ganancia.

