# KNN

## Instroducción

- Clasificación
- K vecinos cercanos
- ¿Como hacer comparaciones?
- Distancia
- Otro tipo de variable
- Hamming


## Clasificación

En  matching learning un metodo sencillo de clasificación y regresión es KNN en lo cual es un metodo no parametrico para determina un conjunto de los K vecinos mas cercanos al que se aparece en lo mas cercano.

K vecinos es un algoritmo que se base en usar los datos más parecidos para poder clasificar un objeto.

![alt text](knn-clasificacion.png)

Buscar a que objeto pertence el verde

## K vecinos cercanos

- La pregunta que uno deberia realizar es: ¿A que clase es la mas parecida? 1 o 2

Se debe definir un noción de distancia entre datos asumiendo que en los datos más parecidos son los que están más cerca en el espacio de las variables x y y.

![alt text](knn-distancia.png)

![alt text](knn-distancia-cercano.png)

## ¿Como hacer comparaciones?

En general los vecinos cercanos determina la distancia mas cercano a K-menos vecinos cercano. Por lo tanto se considera el voto de los K instancia que mas se paresca en cuestión.

![alt text](knn-distancia-cercano-k.png)

Existen casos que no es tan trivial encontrar una clase, el problema es que cada uno de los k vecinos pertence a una clase diferente por lo tanto no hay claridad sobre la clasificación que se deberia proponer, por lo tanto se debe utilizar otro criterio de selección: aquella que sea de menor distancia entre los dos vecinos cercano.

![alt text](knn-distancia-cercano-compracion-distancia.png)

## Distancia

### Distancia Euclideana

Es la distancia entre dos puntos en el espacio, pero tambien puede extenderse a dimensiones mayores dado a dos puntos (q,p).

![alt text](knn-distancia-euclideana.png)

### Distancia Manhattan

Corresponde a la suma de valores absolutos de la diferencia de cada coordenada a ser extensible a D dimensiones.

![alt text](knn-distancia-manhattan.png)

## Otro tipo de variable

Otro tipo de variables que se pueden ver son aquellas que no son numericas y se deben transformar, en los cuales son:

- dos valores (valores binarias): Femenimo - 0 | Masculo - 1 
    - Lo mas comun al momento de trabajar don dos valores es utilizar la distancia de hamming para determinar cercania, returnando 1 (si es que son distintos) o 0 (son iguales).

![alt text](knn-otros.png)

## Hamming

- Agrupa todas las variables
- Calcular la distancia de hamming entre los conjuntos de variables.

Ejemplo: d(hombre, mujer) = 1

Femenino - 0
Masculino - 1

![alt text](knn-otro-hamming.png)

Ejemplo: d(hombre, mujer) = 0

Masculino - 1
Masculino - 1

![alt text](knn-otro-hamming-igual.png)

Hamming calcula la distancia entre un conjunto de variable que puede ser agrupada como vectores:
- Agrupar todas las variables.
- Calcular la distancia de Hmming entre los conjuntos de variables.

Ejemplo:
d(010, 101) = 3

Binario : 010
- Femenino
- Si trabaja
- No tiene hijos

Binario : 101
- Maculino
- No trabaja
- Si tiene hijos

Distancia de himming = 3

![alt text](knn-otro-hamming-ejemplo.png)

## Ejemplo

| Nombre     | Propiedades  | Ingreso Mensual   | Dispuesto a arriesgarse   |
|------------|--------------|-------------------|---------------|
| Antonio    |  0.82        |       0.20        | Si            |
| Magdalena  |  0.18        |       0.31        | No            |
| Javiera    |  0.27        |       0.00        | No            |
| Nicolás    |  1.00        |       0.36        | Si            |
| Agustín    |  0.00        |       1.00        | Si            |

distancia(Antonia, Magdalena) = dif (0.82;0.18)+dif/(0,2:0.31)+dif(Si;No)
= 0.64+0.11+1
= 1.75

### Variables categorícas

1. Cantidad fijas de valores.
2. No tienen un orden defnidos.
3. Cáculo de distancia entre variables categóricas.


| Nombre     | Propiedades  | Ingreso Mensual   | Dispuesto a arriesgarse   |
|------------|--------------|-------------------|---------------|
| Antonio    |  Entre 1 y 4 |       Alto        | Si            |
| Magdalena  |  Mas de 4    |       Medio       | Si            |

![alt text](knn-variables-categoricas.png)

### Consideraciones

Se debe tener en consideración la cantidad de valores posibles que puede tomar una variable categórica porque mas valores la variable puede tomar, meno probabilidad que distintos objetos tenga el mismo valor en esa variable. 

Ejemplo:
A sumir que existen 100 posible rango de propiedades y 4 posibles ingreso.

![alt text](knn-variables-categoricas-consideraciones.png)

### Variables ordinales

Las variables ordinales tienen un orden definidos y tiene un cantidad fija de valores 
y tienen algo en común en mapear los valores de minimo a maximo entre 0 y 1. 

![alt text](knn-variables-ordinales.png)

# Síntesis

- Es un algoritmo de aprendizaje supervisado, es decir que apartir que a partir de un juego inicial su objetivo sera clasificar todas las instancia nuevas.
- Usa métrica de distancia para calcular vecinos cercanos.
- Tiene gran uso en sistemas de recomendaciones (plaforma de contenido digital, proceso de venta cruzada en comercio electronico, etc).

