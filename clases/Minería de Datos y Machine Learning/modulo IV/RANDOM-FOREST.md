# Instroducción

- ¿Que es random forest?
- ¿Cómo trabaja random forest?
- Ejemplo de uso
- Ejemplo
- Ventajas
- Desventajas

## ¿Que es random forest?

Es un método verstail de aprendizaje automático para realizar tanto tareas de regresión como de clasificación.

![alt text](que-es-random-forest.png)

Tambien lleva a cabo método de reducción dimensionalidad trata valores perdidos, valores atípicos y otros pasos esenciales de exploración de datos.

## ¿Cómo trabaja random forest?

En Random Forest realiza la ejecución de varios algoritmos de árbol de decisiones en lugar de uno solo

![alt text](que-es-random-forest-como-trabaja.png)

Para clasificar un nuevo objeto basado en atributo cada árbol de decisión de una clasificación y la desición con mayor "votos" es la predicción del algoritmo. Para ello se utiliza la tecnica  bagging que crear sub set de datos.

bagging: Es una tecnica usada para reducir la varianza de la predicciones atravez de la combinación de los resultados de varios clasificadores. Cada uno de estos clasificadores son modelado con diferente sub conjunto tomado de la misma población.

![alt text](que-es-random-forest-sub-conjunto.png)

Una vez generado los subset de datos, se crea los árboles aleatorios atravez de un subconjunto de caracteristicas como se muestra en la imagen anterior.

![alt text](que-es-random-forest-sub-conjunto-resultado.png)

## Ejemplo de uso

Los bosque aleatorio o random forest se han implementado en diversas áreas en algunos aplicaciones incluyen:

- Reconocimiento de imagenes (clasificación de imagenes de objetos en general)
- Remote sensing (adquisición de información a distancia)
- Biología molecular (analisis de secuencias de amonoacidos)
- Astronomia (Clasificación de galaxias, etc.)

## Ventajas

- Evita el sobreajuste
- Uno de los modelos de decisión más precisos.
- Funciona bien en grandes conjunto de datos.
- Manejar miles de variables de entrada.
- Identificar las variables más significativas.

## Desventajas

- Sobreajuste en caso de datos ruidosos. 
- Resultados deficiles de interpretar.
- Tarda más tiempo en crear de todos los árboles para la creación de la clasificació final.

## Sintesis

- En general es de fácil interpretación.
- Puede manjear hasta un amplio número de variables de entrada e identificar las más significativas.
- Provee una visión resumida y global del comportamiento del modelo.
- Incorporar metodos efectivos para estimar valores  faltantes en el set de dats.
- Cada árbol de una clasificación.
- Es posible usarlo como método no supérvisado (clustering) y detección de outliers.

