# Mineria de datos

## ¿ Qué es la mineria de datos ? 
Es el estudio de recopliar, limpiar, procesar, analisar y haci obtener una categoria de datos para realizar una respresentación de una aplicación real de mineria de datos.

## Caracteristicas
- Pertence a ciencias de la computación y estadistica
- Ek estudio del desarrollo de herramientas automáticas para analizar datos.
- Las soluciones deber: suficientesmente rápidas (big data), suficientemente precisas (modelos flexibles y adaptativos).

## Analogía
- Minería para extracción de oro.
- Minería de datos desde fuentes de información (2025 10 veces mayor).

## KDD y metodología CRIISP-DM

### KDD
Proceso metodologíco para identificar patrones válidos, novedodos, potencialmente útiles y entendibles end datos.

![alt text](KDD.png)

#### Etapas
- Selección de datos.
- Procesamientos de datos.
- Transformación de los datos.
- Minería de datos.
- Obtención de Patrones.
- Evalución.
- Concomiento de esta evaluación.

### metodología CRIISP-DM
Se crea posterior de KDD, del ciclo de vida de un proyecto. Es un estándar de factor del mercado para el análisis de datos.
- Cross-Industry Standard Process for data mining.
- Método para orientar proyectos de minería de datos.
- Estándar de factor del mercado.

![alt text](CRISP-DM.png)

#### Etapas
- Comprensión del negocio (definición de las necesidades del cliente)
- Comprensión de los datos (estudio y comprensión de los datos)
- Preparación de los datos (selección y análisis de características para generar un previo de análisis de datos)
- Modelado (eligen y aplica técnica de modelado basándonos en la problemática)
- Evaluación (se analizá a fondo, basándonos en el objetivo del negocio)
- Despliegue

## Herraimentas de Minería de datos
- Rapidminer
- Orange
- Kmine
- R

## Aplicaciones de uso

### Salud
- Indentificación de tratamientos de salud.
- Clasificación de patologías.
- Análisis de ADN y proteinas.
- Clasificación de nuevas especies.

### Transporte
- Detectar rutas de despacho.
- Analisar patrones de productos.
- Realizar estaciones de entregas.

### Banca
- Desctar fuga de clientes
- Detección de fraude.
- Segmentación de clientes.
- Análisis de conductas en transacciones.

### Marketing y comercio
- Analisis de compras.
- Segmentación de clientes.
- Predcción de conductas de clientes.
- Recomendación de compras.

## Problemas con los datos dispersos
- No se puede obtener el dato que se necesita.
- No se puede entender el dato encontrado.
- No se puede buscar el dato que se necesita.
- No se puede usar el dato encontrado.

### Base de datos transaccionales
- Cubre aspectos operaciones de una organización.
- Más comunes de hoy en día.
- On-line transaction processing systems (OLTP).

### Base de datos analíticas
Business Intelligence, Data Mining, Customer Relation Management (CRM).

- Cubren aspectos estratégicos de una organización.
- Utilizados para analizar información en búsqueda de conocimiento relevante.
- Cada vez más comunes.
- On-line analytical processing systems (OLAP).

![alt text](OLPT-OLAP.png)

### Data Warehouse
Una colección de datos orientda al negocio, integrada, variante en el tiempo y no volátoñ, que tienen por objetivo dar soporte a la toma de decisiones de la gerencia.

![alt text](DataWarehouse.png)

#### ¿ Por qué usar una Data Warehouse?
- Facilidad de acceso a la información.
- Mayor flexibilidad y rapidez de respuesta
- Consolidación y homogeneización de la información.
- Mejor comunicación entre departamentos de la empresa.
- Entrega de información relevante que antes no se almacenaba.
- Obtención de una base confiable para aplicar técnicas de analytics.

![alt text](Data-Warehouse-proceso.png)
![alt text](Data-Warehouse-modelación.png)
![alt text](Data-Warehouse-ingegracion.png)
![alt text](Data-Warehouse-disenio.png)

### Data Cube

- Slicing: seleccionar un miembro de una dimensión de un cubo
- Dicing: seleccionar varios miembros de varias iniciales de un mini cubo.

![alt text](Data-Warehouse-Data-Cube.png)

## Data mart
- Como una DW pero mas ppequeña y específica.
- Dedicada a un departamento particular de la compañia.
- Problema frecuente hererogeneidad de cada departamento.

### Roll up´y Drill Down
- La navegación del cubo, se puede navegar sin la nacesidad de intrucción sql.

![alt text](roll-up-drill-down.png)

### ¿Como se guardan los datos?

- Tabla de dimensión: corresponde a un objeto o concepto del mundo real.
    - Ejemplo
        - Consumidor
        - Producto
        - Día
        - Empleados
        - Regiones
        - Tiendas
    - Propiedades
        - Contiene varias columnas descriptivas.
        - No tiene muchas filas
        - Son datos estaticos.
- Tablas de hechos: contiene mediciones acerca de un evento en un proceso de interés.
    - Ejemplos
        - Venta
        - Insumo
    - Cada fila 2: tipo de datos
        - Columnas con valores numéricos o mediciones.
        - Llaves a tablas de dimensiones.
    - Propiedades
        - Gigantes: a menudos millones o billones de filas.
        - Angostas: a menudo pocas columnas.
        - Cambian frecuentemente.

### Modelos DW
- Modelo estrella: Tablas sin relaciones de la tabla hechos.
    - Menos consumo.
    - No es necesario JOIN

![alt text](modelo-estrella.png)

- Modelo copo de nieve (snowflake): clasificar claramente sus relaciones.
    - Mayor consumo.
    - Es necesario JOIN

![alt text](modelo-copo-de-nieve.png)

https://aml.stradata.co/preprocesamiento-de-datos-una-forma-de-solucionar-problemas-antes-de-que-aparezcan/