# Reglas de asociación

## ¿ Qué son las reglas de asociación ?

Técnicas de Minerias de datos para encontrar asociaciones o correlaciones entre los elementos u objetos de una base de datos transaccionales, relacionales o data-warehouse.

## Origen

Contexto de datos de supemercados que contienen conjuntos de artículos comprados por los clientes, llamadas *transacciones*.

El objetivo principal se determina en la *asociacion entre grupos de artículos comprados por los clientes*, *correlacionado entre artículos*.

## Aplicaciones de reglas de asociación

1. Relación de producto en compra de artículos.
2. Segmentación.
3. Promoción entre pares de producto.
4. Ordenamiento de productos.
5. Mineria de texto.

## ¿ Por qué usar reglas de asociación ?

- Métodos de clasificación.
- Clustering.
- Análisis de datos poco frecuentes.

# Definiciones

## Itemset

Es una colección de uno o mas elementos => EJ: (leche, pañales, cerveza)

## Soporte

Es la frecuencia relativa que un itemset aparece en la base de datos => EJ: soporte = 3 / 10 

*Número de itemset que aparece en la base de datos de compra dividido por el número totales de compras (transacciones).*

## Itemset frecuente

Es aquel que aparece en una frecuencia mayor a un umbral, donde el umbral esta difinido.

## Regla de asociación

Es una expresión de la forma  => x -> y
donde *x* e *y* son itemsets

*x* = Antecedente
*y* = consecuente

Ejemplo: {leche, pañales} -> {cerveza}

*Si se compra leche y pañales, es posible que tambien se compra cerveza*

## Ejemplo de transacciones

![alt text](ejemplo-de-transacciones.png)

## Ejemplo de regla de asociación Antecedente y consecuente
![alt text](ejemplo-de-transacciones_2.png)

# Indicadores de rendimiento

## Soporte

Dada la regla comprar lecha, pañal y cervesa, el soporte se define en la cantidad de itemset en la regla que aparece en el set de datos, dividiendo por la cantidad total de transacciones existen en el set de datos.

Donde el soporte es la cantidad de veces que aparece {leche, pañales, cervesa} en el set de datos, divido por la cantidad total de transacciones.

![alt text](indicador_rendimiento_soporte.png)

## Confianza

La confianza se puede interpretar como un estimador de probabilidad de encontrar la parte derecha de una regla condicionada que también se puede encontrar en la parte izquierda.

![alt text](indicador_rendimiento_confianza.png)

# Problema

Si la confianza es 0.7 para la regla => {leche} -(implica)> {cerveza}. Esto quiere decir que un 70% de probabilidad empírica que el *cliente compre cerveza si compra leche*.

*¿Qué pasa si la pobabilidad a priori de comprar solamente cerveza ya es de un 70%?*

![alt text](problema.png)

## Lift

Esta metrica, permite medir el incremente del lado derecho de la regla (consecuente) data la compra del lado izquierdo (antecedente). De esta menra se puede definir esta metrica como la confianza de la regla dividido por el soporte del consecuente.

![alt text](lift.png)

La confianza de comprar leche y pañales en conjunto implica una probabilidad en un 0.67 y el soporte de la cerveza es un 0.6 implica el lift en un 1.117, es decir que esta probabilidad esta positivamente correlacionada.

![alt text](lift_conjunto_soporte.png)

## Metrica Lift

- Positiva (Lift > 1):

Cuando la metrica lift es positiva indica que la probabilidad del consecuente de la regla aumentó dato que el consumidor compró los items del antecedente. El antecendete y consecuente estan correlacionada positivamente.

- Igual a 1 (Lift = 1)

Cuanto la metrica lift es igual a uno, indica que la probabilidad no se vio afectada, es decir, el consecuente no se ve influencido por el antecedente. El antecendete y consecuente son independiente la regla no representa un valor real.

- Negativa (lift < 1)

Cuando la metrica lift es menor a 1, indica que el antecente tuvo un efecto negativo en la ocurrencia del consecuente, lo baja a su probabilidad. Es decir que el antecendente y consecuente estan relacionado negativamente.

(patrones de paginas web, análisis de compra, diagnóstico medicos)