# FP - Growth

## Introducción

### ¿ Qué desventajas tiene Aprori?

- La generación de candidatos puede relativamente lento, por lo cuanto se debe sacar itemset frecuente individuales pares, tripletas, etc.
- El método de conteo itera sobre todas las transacciones en cada iteración. 
- Los ítems string hacen al algoritmo mucho más pesado y genera un alto consumo en memoria.

### Algoritmo (Pasos)

Para desarrollar el algoritmo Fp-Growth, considera lo siguiente:
1. Escanear la base de datos para encontrar la concurrencia de los conjuntos de elementos en la DB.
2. Contruir el árbol FP.
3. Revisar la base de datos y examinar las transacciones y los conjuntos de elementos ordenando de forma descendente.
4. Minar el árbol FP
5. Incremento en el número de itemset
6. Examinar la siguientes transacciones en la base de datos.
7. construir el árbol FP condicional
8. Encontrar patrones generado por el árbol FP condicional.

![alt text](pasos-fp-growth.png)

### Ejemplo

Set de datos transaccionales
 
| id        | transaccion               |
|-----------|---------------------------|
|       1   |   I1, I2, I3, I4, I5, I6  |
|       2   |   I7, I2, I3, I4, I5, I6  |
|       3   |   I1, I8, I4, I5          |
|       4   |   I1, I9, I10, I4, I6     |
|       5   |   I10, I2, I4, I11, I5    |   

![alt text](set-datos-fp-growth.png)

### Obtención de metricas de soporte

| Soporte   | id transaccion    |            |                          |
|-----------|-------------------|------------|--------------------------|
|I1         |   3       [OK]    |    1       |   I1, I2, I3, I4, I5, I6 |
|I2         |   3       [OK]    |    2       |   I7, I2, I3, I4, I5, I6 |
|I3         |   2               |    3       |   I1, I8, I4, I5         |
|I4         |   5       [OK]    |    4       |   I1, I9, I10, I4, I6    |
|I5         |   4       [OK]    |    5       |   I10, I2, I4, I11, I5   |
|I6         |   3       [OK]    |            |                          |
|I7         |   1               |            |                          |
|I8         |   1               |            |   Umbral = 3 / 11        |
|I9         |   1               |            |                          |
|I10        |   2               |            |                          |
|I11        |   1               |            |                          |

![alt text](item-sobre-umbral-fp-growth.png)

### Ordenamiento

Encontrar el set L con frecuencia mas frecuentes, llamado tambien patrón frecuente conformado I4, I5, I1, I2, I6 ordenado de mayor a menor.

| Items | Soporte | id transacción | Items                      | itemset ordenado   |
|-------|---------|----------------|----------------------------|--------------------|
|   I5  |   4     |      1         | I1, I2, I3, I4, I5, I6     | I4, I5, I1, I2, I6 |
|   I1  |   3     |      2         | I7, I2, I3, I4, I5, I6     | I4, I5, I2, I6     |
|   I2  |   3     |      3         | I1, I8, I4, I5             | I4, I5, I1         |
|   I4  |   5     |      4         | I1, I9, I10, I4, I6        | I4, I1, I6         |
|   I6  |   3     |      5         | I10, I2, I4, I11, I5       | I4, I5, I2         |

![alt text](ordenamiento-fp-growth.png)

### Construir del árbol FP

Se construye un node raiz llamado null, luego se crea un nodo L4:1, L5:1 e ir sumando sus nodos.

| Items | Soporte | id transacción | Items                      | itemset ordenado   |
|-------|---------|----------------|----------------------------|--------------------|
|   I4  |   5     |      1         | I1, I2, I3, I4, I5, I6     | I4, I5, I1, I2, I6 |
|   I5  |   4     |      2         | I7, I2, I3, I4, I5, I6     | I4, I5, I2, I6     |
|   I1  |   3     |      3         | I1, I8, I4, I5             | I4, I5, I1         | 
|   I2  |   3     |      4         | I1, I9, I10, I4, I6        | I4, I1, I6         |
|   I6  |   3     |      5         | I10, I2, I4, I11, I5       | I4, I5, I2         |

![alt text](construccion-del-arbol-fp-growth.png)

### Obtención de patrones frecuentes

|   Id  | transacción            |  Ítems         | Ítemset Ordenados | L Invetido     |
|-------|------------------------|----------------|-------------------|----------------|
|   1   | I1, I2, I3, I4, I5, I6 | I4, I5, I1, I2, I6    |   I6
|   2   | I7, I2, I3, I4, I5, I6 | I4, I5, I2, I6        |   I2
|   3   | I1, I8, I4, I5         | I4, I5, I1            |   I1
|   4   | I1, I9, I10, I4, I6    | I4, I1, I6            |   I5
|   5   | I10, I2, I4, I11, I5   | I4, I5, I2            |   I4

|Patrón condicional(raiz-al-invertido)    | P-Tree condicional (elementos comunes) |
|-----------------------------------------|----------------------------------------|
|{{I4,I5,I1,I2:1},{I4,I5,I2:1}, {I4,I1:1}}|{I4:3}                                  |
|{{I4,I5,I1:1},{I4,I5:2}}                 |{I4,I5:3}                               |
|{{I4,I5:2},{I4:1}}                       |{I4:3}                                  | 
|{{I4:4}}                                 |{I4:4}                                  | 
|0                                        | 0                                      | 

![alt text](obtencion-patrones-frecuentes-fp-growth.png)

### Final de obtención de patrones frecuente

Se busca los itemset frecuentes, usando la columna *L-Invertido* y  *FP-Tree Condicional* a travez de combinaciones.

 
|   Patrones    | Frecuentes                  | 
|---------------|-----------------------------|
|       I6      |   <I4,I6:3>
|       I2      |   <I4,I2:3><I5,I2:3><I4,I5,I2:3>      
|       I1      |   <I1,I4:3>
|       I5      |   <I5,I4:4>

Ahora es posible de encontrar reglas de asociación

![alt text](regla-asociacion-final-fp-growth.png)