# Reglas de asociación – Apriori

## ¿Que es apriori?

Es un algoritmo de encontrar reglas de asociaciones de manera automatica en un set de datos, propuesto en 1993 por Agrawall. Algoritmo capaz de encontrar reglas de asociaciones que cumplan con un mínimo valor de soporte y confianza.

## Pasos de algoritmo Apriori

1. Se calcula el soporte de cada ítem individual, y se determinan los 1-itemsets frecuentes.
2. En cada paso subsecuente, los itemset frecuentes generados en los pasos anteriores se utilizan para generar los nuevos itemsets (itemsets candidatos).
3. Se calcula el soporte de cada ítemset candidato y se determinan los ítemsets frecuentes.
4. El proceso continúa hasta que no pueden ser encontrados nuevos ítemsets frecuentes.

## Principio de monotonicidad

### Encontrando itemsets

La idea es obtener todo los itemsets posibles junto su frecuencia. Ejemplo: Si quisiera juntar 5 itemset a,b,c,d y e, se podría representar visualizándolo en un lattice de itemsets.

![alt text](itemsets-encontrados.png)

### Encontrando itemsets problema

![alt text](itemsets-encontrados-problema.png)

#### Solución:

El principio de monotonicidad ayuda a reducir la cantidad de ítemsets posibles a considerar dentro de nuestros set de datos. Si un itemset es frecuente, todos los subgrupos de este ítemset son frecuentes. Ejemplo: si el ítemset frecuente es {pescado, mayonesa, bebida}, entonces el ítemset {mayonesa, bebida} también sera frecuente.

Análogamente, si el ítemset {queso, galleta} no es frecuente, entonces si agregamos el ítem mayonesa a este ítemset, quedando {queso, galleta, mayonesa}, la frecuencia empezará a bajar.

## Ejemplo

Dado el siguiente set de datos: 

1 - leche, manzana, naranja, pera.

2 - leche, manzana, pera.

3 - manzana, naranja, pera.

4 - naranja, pera, limón, platano.

Si el umbral lo definimos como 3/4
El itemset pera, manzana aparece en conjunto 2 veces, siendo el soporte: S ({pera, manzana}) = 2 / 4 (no supera el umbral)

Si al mismo ejemplo agregamos otro itemset leche, quedando:

s({pera, manzana,leche}) = 1/4
{persa, manzana} no supera el umbral, por lo tanto, si se agrega otro ítem, tampoco lo superará.

## Generación de ítemset candidatos

### Inicio de algoritmo

1. El algoritmo apriori, primeramente obtiene los itemset frecuentes.
2. Luego calcula las reglas de asociación a partir de ellos. Una vez generada las reglas, se descartán las que no cumplan con un umbral de soporte de confianza.

![alt text](itemsets-apriori-inicio.png)

### Ejemplo set de datos

1 - I1, I2, I3, I4

2 - I3, I4

3 - I2, I3, I4

4 - I3, I4, I5, I6

Para comenzar se asume que todos los productos son candidatos a ser item frecuente de manera individual.

I1, I2, I3, I4, I5, I6 - Del set de datos son item individual clasificados.

A continuación se debe eliminar los itemsets que no superen un umbral determinado: 

Umbral determinado : 2 / 4

I1, I2, I3, I4, I5, I6 

I1 = 1 / 4 [NO]

I2 = 2 / 4 [SI]

I3 = 4 / 4 [SI]

I4 = 4 / 4 [SI]

I5 = 1 / 4 [NO]

I6 = 1 / 4 [NO]

![alt text](seleccion-itemset-individual.png)

A continuación se debe generar los itemsets que tengan 2 items considerando los ítems seleccionados anteriormente.

I2 = 2 / 4 [SI]
I3 = 4 / 4 [SI]
I4 = 4 / 4 [SI]

![alt text](seleccion-itemset-2-individual.png)

A continuación se eliminan los itemset que no superen un umbral determinado sobre los valores clasificado. En lo cual se toma los itemset clasificado y verificar cuanta veces se repiten en las transacciones.

I2, I3, I4 = 2 / 4 [SI]

![alt text](seleccion-itemset-3-individual.png)

- No hay más elementos que combinar.
- Algoritmo se detiene en la búsqueda de itemset candidatos.

## Visto el ejemlo ¿Cómo construir los ítemsets?

### Regla

Definir un orden lexicográfico (arbitrario) entre los productos del set de datos de transacciones. En el siguientes ejemplo, se ha ordenado los itemset de menor a mayor (lexicográfico).

![alt text](regla.png)

### ¿Cuándo se pueden combinar 2 ítemsets?

Para que esto suceda, los itemsets deben tener la misma cantidad de elementos.

### Ejemplo

itemset A       Itemset B
I2,I4,I3        I3,I2,I6
- Ordenar lexicografico
I2,I3,I4        I2,I3,I6

- Se debe comprar los items
- De izquierda a derecha, elemento por elementos que sean iguales.
- Menos en la última posición

![alt text](posicion-abitrario-inicial.png)

Al tener el ordenamiento abitrario, se debe generar un nuevo itemset con la operación JOIN (0)

![alt text](posicion-abitrario-inicial-join.png)

# Ejemplo General

![alt text](ejemplo-general-1.png)

![alt text](ejemplo-general-2.png)
- Generar el conjunto de itemset individuales.
- Evaluar el soporte de cada uno.

![alt text](ejemplo-general-3.png)
- Formación del conjunto frecuente de tamaño 1.
- Generando nuevo itemset de tamaño 2 desde L1-1.

![alt text](ejemplo-general-4.png)
- Se evalua el soporte de cada uno de los candidatos.

![alt text](ejemplo-general-5.png)
- Del paso anterior, se genera el conjunto de itemset frecuente de tamaño2.
- Se debe generar nuevos itemset de tamaño 3 desde L2.
- C2 = conjunto nuevo del join

![alt text](ejemplo-general-6.png)
- De los cantidatos anteriores conformado por el join, se refleja el conjunto de itemset que cumpla con el umbral

![alt text](ejemplo-general-8.png)
- L3 que conformado los itemset de 3
- El item set de cuatro esta formado por {I1,I2,I3}

![alt text](ejemplo-general-7.png)
- No existe subconjutno

![alt text](ejemplo-general-9.png)


# Síntesis
- Esta técnica permite encontrar conjuntos de items frecuentes.
- Se basan en medidas de confianza y soporte contradas.
- El principio de monotonicidad permite reducir el espacio de búsqueda.
