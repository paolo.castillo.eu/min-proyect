import os
import pymongo
import configparser

config = configparser.RawConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '../config', 'config.ini') )

class DB:
    __getdbst__ = config["DEFAULT"]["string_connection"]
    __getdb__ = config["DEFAULT"]["database"]
    __getcol__ = config["DEFAULT"]["collection_government"]+config["DEFAULT"]["anio"]
    
    def __init__(self):
        connection_string = self.__getdbst__
        self.client = pymongo.MongoClient(connection_string)
        self.database = self.client[self.__getdb__]
        self.collection = self.database[self.__getcol__]
        
    def insert_one(self, data):
        """ Insert data en MongoDB (atributo:valor) """
        try:
            self.collection.insert_one(data)
            return True
        except Exception as e:
            print("An exception ocurred:", e)
            return False
        
    def insert_many(self, data):
        """" Insert data en MongoDB (arreglos) """
        try:
            self.collection.insert_many(data)
            return True
        except Exception as e:
            print("An exception ocurred:", e)
            return False
    
    def drop_collection(self):
        """ Eliminar colección en MongoDB en una DB """
        try:
            self.database.drop_collection(self.__getcol__)
            return True
        except Exception as e:
            print("An exception ocurred:", e)
            return False

if __name__ == '__main__':
    print(f"DB: {config['DEFAULT']['database']}")
    print(f"COL: {config['DEFAULT']['collection_government']}")