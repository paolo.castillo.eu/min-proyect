import os
import json
import requests
import configparser

config = configparser.RawConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '../config', 'config.ini') )


class ListResource():
    
    __getLink__ = config['DEFAULT']['news_api_web']+config["DEFAULT"]["anio"]
    __setHeaders__ = { "user-agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36" }
    
    def __init__(self):
        self.link = self.__getLink__
    
    def getResource(self):
        req = []
        try:
            req = requests.get(self.link, headers=self.__setHeaders__)
            if req.status_code == 404:
                return []
            res = json.loads(req.content.decode("utf-8"))
        except:
            res = []
        return res

if __name__ == '__main__':
    print(f"API: {config['DEFAULT']['news_api_web']}")