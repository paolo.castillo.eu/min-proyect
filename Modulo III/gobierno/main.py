import time
from api.resources import ListResource
from db.database import DB

__db__ = DB()

def add_anios_collection():
    if (__db__.drop_collection()):
        api = ListResource()
        return __db__.insert_many(api.getResource())
    return False

def get_all():
    for holidays in __db__.collection.find():
        print(f"El día de {holidays['nombre']} es un feriado de tipo {holidays['tipo']} y se celebra el {holidays['fecha']}.")
 
def get_type_civil():
    for holidays in __db__.collection.find({"tipo" : "Civil"}):
        print(f"El día de {holidays['nombre']} es un feriado de tipo {holidays['tipo']} y se celebra el {holidays['fecha']}.")
    
def get_day_inalienable():
    for holidays in __db__.collection.find({"irrenunciable" : "1"}):
        print(f"El día de {holidays['nombre']} es un feriado de tipo {holidays['tipo']} y se celebra el {holidays['fecha']}.")

def get_find_holy_work():
    for holidays in __db__.collection.find({"nombre" : {"$regex":"\w*Santo\w*", "$options" : "i"}}):
        print(f"El día de {holidays['nombre']} es un feriado de tipo {holidays['tipo']} y se celebra el {holidays['fecha']}.")

def get_find_holy_work():
    for holidays in __db__.collection.find({"nombre" : {"$regex":"\w*Santo\w*", "$options" : "i"}}):
        print(f"El día de {holidays['nombre']} es un feriado de tipo {holidays['tipo']} y se celebra el {holidays['fecha']}.")

def get_find_plebiscite_holy():
    print("Las leyes involucradas en el día del Plebiscito Constitucional son las siguientes:")
    for holidays in __db__.collection.find({"nombre" : {"$regex":"\w*Plebiscito Constitucional\w*", "$options" : "i"}} , {"_id":0, "leyes":1}):
        for law in holidays["leyes"]:
            print(f"{law['nombre']} Revisar en: {law['url']}.")
    
list = {
    1: ("===== Todos los Feriados de 2020 =====", get_all),
    2: ("===== Solo los Feriados Civiles de 2020 =====", get_type_civil),
    3: ("===== Solo los Feriados Irrenunciables de 2020 =====", get_day_inalienable),
    4: ('===== Solo los Feriados que incluyen "Santo" o "Santos" =====', get_find_holy_work),
    5: ("===== Leyes relacionadas con el Plebiscito de Abril =====", get_find_plebiscite_holy),
    
}

if __name__ == '__main__':
    print("1.- Obtener desde la api indicada todos los feriados del 2020")
    if add_anios_collection():
        time.sleep(2)
        """consultas a la base de datos:"""
        for i, v in list.items():
            time.sleep(2)
            print(v[0])
            v[1]()
            