# plugin VS Code MySql
import os
from csv import reader
import mysql.connector as db

class File:
    
    def __init__(self):
        self.name = "cl-airports.csv"

    def getPath(self):
        return os.path.join(
            os.path.dirname(__file__), self.name)

    def read(self):
        rows = []
        file = open(self.getPath(), "r", encoding="utf-8")
        csvFile = reader(file, delimiter=";")
        next(csvFile)
        for line in csvFile:
            rows.append(line)
        return rows

class DB:
    table = "create table if not exists Aeropuertos \
        (id int primary key, ident varchar(80), type varchar(80), name varchar(80), elevation_ft int, municipality varchar(80), iata_code varchar(80), score int)"
    
    def __init__(self, **config):
        self.connection = None
        self.connection = db.connect(
            host= config['host'], 
            user= config["user"], 
            passwd= config["passwd"], 
            database= config["database"],
            auth_plugin="mysql_native_password"
        )
        self.query(self.table, None)
        
    def query(self, sql, args):
        cursor = self.connection.cursor()
        cursor.execute(sql, args)
        return cursor

    def insert(self, sql, args):
        cursor = self.query(sql, args)
        id = cursor.lastrowid
        self.connection.commit()
        cursor.close()
        return id
    
    def fetch(self, sql, args):
        rows = []
        cursor = self.query(sql, args)
        if cursor.with_rows:
            rows = cursor.fetchall()
        cursor.close()
        return rows
  
    def __del__(self):
        if self.connection != None:
            self.connection.close()  
rows = [
    (39340, 'SHCC', 'heliport', 'Clínica Las Condes Heliport', 2461, 'Santiago', '', 25),
    (39379, 'SHMA', 'heliport', 'Clínica Santa María Heliport', 2028, 'Santiago','', 25),
    (39390, 'SHPT', 'heliport', 'Portillo Heliport', 9000, 'Los Andes', '', 25),
]

if __name__ == "__main__":
    file = File()
    db = DB(host="127.0.0.1", user="root", passwd="archer", database="InfoAeropuertos")
    db.query("delete from Aeropuertos", None)
    
    query = "insert into Aeropuertos \
        (id, ident, type, name, elevation_ft, municipality, iata_code, score) \
            values \
                (%s, %s, %s, %s, ifnull(nullif(%s, ''), 0), %s, %s, %s)"    

    for row in file.read():
        db.insert(query, tuple(row))
        
    for row in rows:
        db.insert(query, row)

    query = "select name, type, municipality, elevation_ft \
        from Aeropuertos where elevation_ft > 5000"
    
    for row in db.fetch(query, None):
        print(row)
    
    