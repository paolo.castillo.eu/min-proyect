# -*- coding: utf-8 -*-
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %      
% PROYECTO 1: CLASS OBJECT                                              %
%                                                                       %
% --------------------------------------------------------------------- %
%                                                                       %
% Created on Aug 23 19:37:41 2021                                       %
% @author: Paolo Castillo                                               %
% https://gitlab.com/paolo.castillo.eu/min-proyect                      %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

import parametros as p
import random

###### INICIO PUNTO 1 ######
### Rellenar Clase Automóvil ###
class Automovil():
    
    def __init__(self, kilometraje, ano):
        self.__kilometraje = kilometraje
        self.ano = ano
        self.ruedas = []
        self.aceleracion = 0
        self.velocidad = 0
    
    # hora / s 3600
    def avanzar(self, tiempo):
        self.__kilometraje += self.velocidad * tiempo
    
    # tiempo en segundo
    def acelerar(self, tiempo):
        self.aceleracion = tiempo * 0.5
        self.velocidad += self.aceleracion * tiempo * 3.6
        self.avanzar(tiempo)
        self.aceleracion = 0
    
    # tiempo en segundo
    def frenar(self, tiempo):
        self.aceleracion -= tiempo * 0.5
        self.velocidad += self.aceleracion * tiempo * 3.6 if self.velocidad > 0 else 0
        self.avanzar(tiempo)
        self.aceleracion = 0
        
    def obtener_kilometraje(self):
        return self.__kilometraje
    
    def reemplazar_rueda(self):
        print(self.ano)
        x = len(self.ruedas)
        # print(min(_ruedas, key=lambda r: r.resistencia_actual).resistencia_actual)
        print(min(
            self.ruedas, key=lambda r: r.resistencia_total).resistencia_total)
        print(min(
            self.ruedas, key=lambda r: r.resistencia_actual).resistencia_actual)
        self.ruedas = [rueda for rueda in self.ruedas if rueda.resistencia_actual != min(
            self.ruedas, key=lambda r: r.resistencia_actual).resistencia_actual]
        for r in range(len(self.ruedas), x):
            self.ruedas.append(Rueda())
        
###### FIN PUNTO 1 ######


###### INICIO PUNTO 2 ######
### Rellenar Clase Moto ###
class Moto(Automovil): 
    
    def __init__(self, kilometraje, ano, cilindrada):
        super().__init__(kilometraje, ano)
        self.cilindrada = cilindrada
        self.ruedas = [Rueda(), Rueda()]

    # tiempo en segundo
    def acelerar(self, tiempo):
        super().acelerar(tiempo)
        for rueda in self.ruedas:
            rueda.gastar("acelerar")
    
    # tiempo en segundo
    def frenar(self, tiempo):
        super().frenar(tiempo)
        for rueda in self.ruedas:
            rueda.gastar("frenar")
    
    def __str__(self):
        return f"Moto del año {self.ano}."
###### FIN PUNTO 2 ######


###### INICIO PUNTO 3 ######
### Rellenar Clase Camión ###
class Camion (Automovil):
    
    def __init__(self, kilometraje, ano, carga):        
        super().__init__(kilometraje, ano)
        # Se mide en 𝑘g
        self.carga = carga
        self.ruedas = [Rueda(), Rueda(), Rueda(), Rueda(), Rueda(), Rueda()]

    # tiempo en segundo
    def acelerar(self, tiempo):
        super().acelerar(tiempo)
        for rueda in self.ruedas:
            rueda.gastar("acelerar")

    # tiempo en segundo
    def frenar(self, tiempo):
        super().frenar(tiempo)
        for rueda in self.ruedas:
            rueda.gastar("frenar")
    
    def __str__(self):
        return f"Camión del año {self.ano}."
###### FIN PUNTO 3 ######


### Esta clase está completa, NO MODIFICAR ###
class Rueda:
    def __init__(self):
        self.resistencia_actual = random.randint(*p.RESISTENCIA)
        self.resistencia_total = self.resistencia_actual
        self.estado = "Perfecto"

    def gastar(self, accion):
        if accion == "acelerar":
            self.resistencia_actual -= 5
        elif accion == "frenar":
            self.resistencia_actual -= 10
        self.actualizar_estado()

    def actualizar_estado(self):
        if self.resistencia_actual < 0:
            self.estado = "Rota"
        elif self.resistencia_actual < self.resistencia_total / 2:
            self.estado = "Gastada"
        elif self.resistencia_actual < self.resistencia_total:
            self.estado = "Usada"

### Esta funcion está completa, NO MODIFICAR ###
def seleccionar():
    for indice in range(len(vehiculos)):
        print(f"[{indice}] {str(vehiculos[indice])}")

    elegido = int(input())
    if elegido >= 0 and elegido < len(vehiculos):
        vehiculo = vehiculos[elegido]
        print("Se seleccionó el vehículo", str(vehiculo))
        return vehiculo
    else:
        print("intentelo denuevo.")

###### INICIO PUNTO 4.2 ######
### Se debe completar cada opción según lo indicado en el enunciado ###
def accion(vehiculo, opcion):
    if opcion == 2: #Acelerar
        print(f"{opcion} - Acelerar")
        tiempo = getInput()
        vehiculo.acelerar(tiempo)
        print(f"Se ha acelerado por {tiempo} segundos llegando a una velocidad de {vehiculo.velocidad} km/h.”")
    elif opcion == 3: #Frenar
        print(f"{opcion} - Frenar")
        tiempo = getInput()
        vehiculo.frenar(tiempo)
        print(f"Se ha frenado por {tiempo} segundos llegando a una velocidad de {vehiculo.velocidad} km/h.”")
    elif opcion == 4: #Avanzar
        print(f"{opcion} - Avanzar")
        tiempo = getInput()
        vehiculo.avanzar(tiempo)
        print(f"Se ha avanzado por {tiempo} segundos a una velocidad de {vehiculo.velocidad} km/h.”")
    elif opcion == 5: #Cambiar Rueda
        print(f"{opcion} - Cambiar Rueda")
        vehiculo.reemplazar_rueda()
    elif opcion == 6: #Mostrar Estado
        print(f"Año: {vehiculo.ano}" +
              f"\nVelocidad: {vehiculo.velocidad}" +
              f"\nKilometraje: {vehiculo.obtener_kilometraje() } ")
        for i in vehiculo.ruedas:
            print(i.estado)

def getInput():
    try:
        tiempo = int(input(f"Ingrese el tiempo de aceleración:\n"))
        return tiempo
    except ValueError:
        print("Debe ser valor numerico")
        getInput()
            
###### FIN PUNTO 4.2 ######


if __name__ == "__main__":

    ###### INICIO PUNTO 4.1 ######
    ### Aca deben instanciar los vehiculos indicados
    ### en el enunciado y agregarlos a la lista vehiculos
    moto = Moto(0, 2015, 120)
    camion = Camion(0, 2020, 10)
    
    vehiculos = [moto, camion]


    ###### FIN PUNTO 4.1 ######


    ### El codigo de abajo NO SE MODIFICA ###
    vehiculo = vehiculos[0] # Por default comienza seleccionado el primer vehículo, si no hay vehículos instanciados esto tirará error.

    dict_opciones = {1: ("Seleccionar Vehiculo", seleccionar),
                     2: ("Acelerar", accion),
                     3: ("Frenar", accion),
                     4: ("Avanzar", accion),
                     5: ("Reemplazar Rueda", accion),
                     6: ("Mostrar Estado", accion),
                     0: ("Salir", None)
                    }

    op = -1
    while op != 0:
        
        for k, v in dict_opciones.items():
            print(f"{k}: {v[0]}")
        
        try:
            op = int(input("Opción: "))
        
        except ValueError:
            print(f"Ingrese opción válida.")
            op = -1
        
        if op != 0 and op in dict_opciones.keys():
            if op == 1:
                vehiculo = dict_opciones[op][1]()
            else:
                dict_opciones[op][1](vehiculo, op)
        elif op == 0:
            pass
        else:
            print(f"Ingrese opción válida.")
            op = -1
