##############################################################
from random import choice, sample, randint
## Si necesita agregar imports, debe agregarlos aquí arriba ##

class Restaurante:
    
    def __init__(self, nombre, platos, cocineros, repartidores):
        self.nombre = nombre
        # dict con todos los platos del restaurante
        # cada llave es el nombre del plato y su valor es una lista
        # con el nombre y su tipo
        self.platos = platos
        # lista con los cocineros del restaurante 
        # son instancia de la clase cocinero
        self.cocineros = cocineros
        # lista con los repartidores del restaurante
        # son instancia de la clase repartidor
        self.repartidores = repartidores
        # clasificación del restaurante
        self.calificacion = 0
  
    def recibir_pedidos(self, clientes):
        # clientes es una lista con los objetos de la clase cliente
        # modificar clasificación
        clasificacion = 0
        for cliente in clientes:
            platos_favoritos = cliente.platos_preferidos
            pedido = self.generar_preparacion(platos_favoritos)
            tiempo_entrega = self.generar_reparticion(pedido)
            # recibir pedido
            # pedido y tiempo de entrega
            # si el tiempo de entrega es -1 (no dispone de reponedor)
            # usar una lista vacia
            pedido = [] if tiempo_entrega == -1 else pedido
            # recibir_pedido recibe clasificación del cliente
            # sumar el tiempo de demora de cada cliente atendido
            clasificacion += cliente.recibir_pedido(pedido, tiempo_entrega)
            print(clasificacion)
        # al finalizar la atención de cliente 
        # calcular la clasificación del restaurante
        self.calificacion = clasificacion / len(clientes)
            
    # genera el pedido según la disponiblidad de cocinero
    def generar_preparacion(self, platos_favoritos):
        cocinero_disponible = []
        for plato in self.obtener_platos(platos_favoritos).items():
            # cocinero = choice(Cocinero)()
            # cocinero = choice([item for item in self.cocineros if item.energia > 0 ])
            cocinero = [c for c in self.cocineros if c.energia > 0 ]
            if len(cocinero) > 0:   
                cocinero_con_energia = choice(cocinero)
                cocinero_disponible.append(cocinero_con_energia.cocinar(plato[1])) 
        
        # return lista de pedidos a repartir
        return cocinero_disponible

    def obtener_platos(self, platos_favoritos):
        return {p: platos_favoritos[p] for p in sample(
            list(platos_favoritos), randint(1, len(platos_favoritos)))}
    
    # genera tiempo de entrega de una reparticion
    def generar_reparticion(self, pedido):
        # repartidor = choice(Repartidor)()
        # repartidor = choice(self.repartidores)
        repartidores_disponible = [r for r in self.repartidores if r.energia > 0 ]
        if len(repartidores_disponible) > 0:
            repartidor = choice(repartidores_disponible)
            return repartidor.repartir(pedido)
        
        return -1
            
if __name__ == "__main__":

    ### Código para probar que tu clase haya sido creada correctamente  ###
    ### Corre directamente este archivo para que este código se ejecute ###
    try:
        PLATOS_PRUEBA = {
        "Pepsi": ["Pepsi", "Bebestible"],
        "Mariscos": ["Mariscos", "Comestible"],
        }
        un_restaurante = Restaurante("Bon Appetit", PLATOS_PRUEBA, [], [])
        print(f"El restaurante {un_restaurante.nombre}, tiene los siguientes platos:")
        for plato in un_restaurante.platos.values():
            print(f" - {plato[1]}: {plato[0]}")
    except TypeError:
        print("Hay una cantidad incorrecta de argumentos en algún inicializador y/o todavía no defines una clase")
    except AttributeError:
        print("Algún atributo esta mal definido y/o todavia no defines una clase")
