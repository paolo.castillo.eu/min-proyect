##############################################################
from random import randint, choice, sample
from .platos import Bebestible, Comestible
## Si necesita agregar imports, debe agregarlos aquí arriba ##

class Persona:
    def __init__(self, nombre):
        self.nombre = nombre

class Repartidor(Persona):
    def __init__(self, nombre, tiempo_entrega):
        super().__init__(nombre)
        # segundo aleatorio entre 20 y 30
        self.tiempo_entrega = tiempo_entrega
        # energia del repartidor numero aleatorio 75 y 100
        self.energia = randint(75, 100)

    def repartir(self, pedido):
        # pedido = list()
        # disminuir energia del repartidor
        factor_tamanio = 5 if len(pedido) <= 2 else 15
        self.energia -= factor_tamanio
        # calcular tiempo de demora de entrega de un pedido
        # equivalente al tiempo de entrega
        factor_velocidad = 1.25 if len(pedido) <= 2 else 0.85
        self.tiempo_entrega *= factor_velocidad
        return self.tiempo_entrega

class Cocinero(Persona):
    def __init__(self, nombre, habilidad):
        super().__init__(nombre)
        # habilidad del cocinero aleatorio entre 1 y 10
        self.habilidad = habilidad
        # energia de la persona aleatorio entre 50 y 80
        self.energia = randint(50, 80)
    
    def cocinar(self, informacion_plato):
        # informacion_plato lista con el nombre y tipo de plato a cocina
        # donde el tipo puede ser Bebestible o Comestible
        # verificar Bebestible o Comestible en base a lista entrante
        plato = Bebestible(informacion_plato[0]) if informacion_plato[1] == "Bebestible" else Comestible(informacion_plato[0])
        self.quitarEnergia(plato)
        # calidad del plato a cocinar Bebestible o Comestible
        factor_calidad = 0.7 if plato.dificultad > self.habilidad else 1.5
        plato.calidad *= factor_calidad
        # lista de los plato
        
        # platos = []
        #for plato in informacion_plato:
            # verificar Bebestible o Comestible en base a lista entrante
            #_instancia = Bebestible(plato[0]) if plato[1] == "Bebestible" else Comestible(plato[0])
            #self.quitarEnergia(_instancia)
            # calidad del plato a cocinar Bebestible o Comestible
            #factor_calidad = 0.7 if _instancia.dificultad > self.habilidad else 1.5
            #_instancia.calidad *= factor_calidad
            # lista de los plato
            #platos.append(_instancia)
        return plato
    
    def quitarEnergia(self, plato):
        if type(plato) == Bebestible:
            bebestibleLista = {"Grade": 10, "Mediano": 8, "Pequeño": 5}
            self.energia -= bebestibleLista[plato.tamano]
        else:
            self.energia -= 15

class Cliente(Persona):
    
    def __init__(self, nombre, platos_preferidos):
        super().__init__(nombre)
        # lista con los nombre de los plato preferido del cliente
        # puede ser entre 1 a 5 platos
        self.platos_preferidos = dict()
        for i in sample(list(platos_preferidos), randint(1, 5)):
            self.platos_preferidos[i] = platos_preferidos[i]
        
    def recibir_pedido(self, pedido, demora):
        # pedido: recibe el pedido que es una lista de objeto Bebestible o Comestible
        # demora: tiempo de demora la entrega de los platos
        clasificacion = 10
        clasificacion /= 2 if len(pedido) < len(self.platos_preferidos) or demora >= 20 else 1
        for plato in pedido:
            clasificacion = self.clasificacionPlato(clasificacion, plato.calidad)
        return clasificacion
    
    def clasificacionPlato(self, classificacion, calidad):
        if calidad >= 11:
            classificacion += 1.5
        elif calidad <= 8:
            classificacion -= 3
        return classificacion

if __name__ == "__main__":
    ### Código para probar que tu clase haya sido creada correctamente  ###
    ### Corre directamente este archivo para que este código se ejecute ###
    try:
        PLATOS_PRUEBA = {
        "Jugo Natural": ["Jugo Natural", "Bebestible"],
        "Empanadas": ["Empanadas", "Comestible"],
        }
        un_cocinero = Cocinero("Cristian", randint(1, 10))
        un_repartidor = Repartidor("Tomás", randint(20, 30))
        un_cliente = Cliente("Alberto", PLATOS_PRUEBA)
        print(f"El cocinero {un_cocinero.nombre} tiene una habilidad: {un_cocinero.habilidad}")
        print(f"El repatidor {un_repartidor.nombre} tiene una tiempo de entrega: {un_repartidor.tiempo_entrega} seg")
        print(f"El cliente {un_cliente.nombre} tiene los siguientes platos favoritos:")
        for plato in un_cliente.platos_preferidos.values():
            print(f" - {plato[1]}: {plato[0]}")
    except TypeError:
        print("Hay una cantidad incorrecta de argumentos en algún inicializador y/o todavía no defines una clase")
    except AttributeError:
        print("Algún atributo esta mal definido y/o todavia no defines una clase")
