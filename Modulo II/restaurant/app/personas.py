##############################################################
from random import randint, choice, sample
from platos import Comestible, Bebestible
## Si necesita agregar imports, debe agregarlos aquí arriba ##

### INICIO PARTE 2.1 ###
class Persona:
    def __init__(self, nombre):
        self.nombre = nombre
### FIN PARTE 2.1 ###

### INICIO PARTE 2.2 ###
class Repartidor(Persona):
    def __init__(self, nombre, tiempo_entrega):
        super().__init__(nombre)
        self.tiempo_entrega = tiempo_entrega
        self.energia = randint(75, 100)

    def repartir(self, pedido):
        factor_tamanio = 5 if len(pedido) <= 2 else 15
        self.energia -= factor_tamanio
        factor_velocidad = 1.25 if len(pedido) <= 2 else 0.85
        tiempo_demora = self.tiempo_entrega * factor_velocidad
        return tiempo_demora
### FIN PARTE 2.2 ###

### INICIO PARTE 2.3 ###
class Cocinero(Persona):
    def __init__(self, nombre, habilidad):
        super().__init__(nombre)
        self.habilidad = habilidad
        self.energia = randint(50, 80)

    def cocinar(self, informacion_plato):
        plato = Bebestible(informacion_plato[0]) if informacion_plato[1] == "Bebestible" else Comestible(informacion_plato[0])
        self.quitarEnergia(plato)
        factor_calidad = 0.7 if plato.dificultad > self.habilidad else 1.5
        plato.calidad *= factor_calidad
        return plato
    
    def quitarEnergia(self, plato):
        if type(plato) == Bebestible:
            bebestibleLista = {"Grande": 10, "Mediano": 8, "Pequeño": 5}
            self.energia -= bebestibleLista[plato.tamano]
        else:
            self.energia -= 15
### FIN PARTE 2.3 ###

### INICIO PARTE 2.4 ###
class Cliente(Persona):
    def __init__(self, nombre, platos_preferidos):
        super().__init__(nombre)
        self.platos_preferidos = platos_preferidos

    def recibir_pedido(self, pedido, demora):
        clasificacion = 10
        clasificacion /= 2 if len(pedido) < len(self.platos_preferidos) or demora >= 20 else 1
        for plato in pedido:
            clasificacion = self.clasificacionPlato(clasificacion, plato.calidad)
        return clasificacion
    
    def clasificacionPlato(self, classificacion, calidad):
        if calidad >= 11:
            classificacion += 1.5
        elif calidad <= 8:
            classificacion -= 3
        return classificacion
### FIN PARTE 2.4 ###

if __name__ == "__main__":

    ### Código para probar que tu clase haya sido creada correctamente  ###
    ### Corre directamente este archivo para que este código se ejecute ###
    try:
        PLATOS_PRUEBA = {
        "Jugo Natural": ["Jugo Natural", "Bebestible"],
        "Empanadas": ["Empanadas", "Comestible"],
        }
        un_cocinero = Cocinero("Cristian", randint(1, 10))
        un_repartidor = Repartidor("Tomás", randint(20, 30))
        un_cliente = Cliente("Alberto", PLATOS_PRUEBA)
        print(f"El cocinero {un_cocinero.nombre} tiene una habilidad: {un_cocinero.habilidad}")
        print(f"El repatidor {un_repartidor.nombre} tiene una tiempo de entrega: {un_repartidor.tiempo_entrega} seg")
        print(f"El cliente {un_cliente.nombre} tiene los siguientes platos favoritos:")
        for plato in un_cliente.platos_preferidos.values():
            print(f" - {plato[1]}: {plato[0]}")
    except TypeError:
        print("Hay una cantidad incorrecta de argumentos en algún inicializador y/o todavía no defines una clase")
    except AttributeError:
        print("Algún atributo esta mal definido y/o todavia no defines una clase")
