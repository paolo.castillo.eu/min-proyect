##############################################################
from random import choice, sample, randint
## Si necesita agregar imports, debe agregarlos aquí arriba ##

### INICIO PARTE 3 ###
class Restaurante:
    def __init__(self, nombre, platos, cocineros, repartidores):
        self.nombre = nombre
        self.platos = platos
        self.cocineros = cocineros
        self.repartidores = repartidores
        self.calificacion = 0

    def recibir_pedidos(self, clientes):
        clasificacion = 0
        for cliente in clientes:
            platos_favoritos = cliente.platos_preferidos
            pedido = self.generar_preparacion(platos_favoritos)
            tiempo_entrega = self.generar_reparticion(pedido)
            tiempo_demora = 0 if tiempo_entrega == -1 else tiempo_entrega
            pedido = [] if tiempo_entrega == -1 else pedido
            clasificacion += cliente.recibir_pedido(pedido, tiempo_demora)
        self.calificacion = clasificacion / len(clientes)

    def generar_preparacion(self, platos_favoritos):
        cocinero_disponible = []
        for plato in self.obtener_platos(platos_favoritos).items():
            cocinero = [c for c in self.cocineros if c.energia > 0 ]
            if len(cocinero) > 0:   
                cocinero_con_energia = choice(cocinero)
                cocinero_disponible.append(cocinero_con_energia.cocinar(plato[1])) 
        return cocinero_disponible
    
    def obtener_platos(self, platos_favoritos):
        return {p: platos_favoritos[p] for p in sample(
            list(platos_favoritos), randint(1, len(platos_favoritos)))}

    def generar_reparticion(self, pedido):
        repartidores_disponible = [r for r in self.repartidores if r.energia > 0 ]
        if len(repartidores_disponible) > 0:
            repartidor = choice(repartidores_disponible)
            return repartidor.repartir(pedido)
        return -1
### FIN PARTE 3 #

if __name__ == "__main__":

    ### Código para probar que tu clase haya sido creada correctamente  ###
    ### Corre directamente este archivo para que este código se ejecute ###
    try:
        PLATOS_PRUEBA = {
        "Pepsi": ["Pepsi", "Bebestible"],
        "Mariscos": ["Mariscos", "Comestible"],
        }
        un_restaurante = Restaurante("Bon Appetit", PLATOS_PRUEBA, [], [])
        print(f"El restaurante {un_restaurante.nombre}, tiene los siguientes platos:")
        for plato in un_restaurante.platos.values():
            print(f" - {plato[1]}: {plato[0]}")
    except TypeError:
        print("Hay una cantidad incorrecta de argumentos en algún inicializador y/o todavía no defines una clase")
    except AttributeError:
        print("Algún atributo esta mal definido y/o todavia no defines una clase")
