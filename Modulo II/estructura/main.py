# -*- coding: utf-8 -*-
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %      
% PROYECTO 1: ESTRUCTURE                                                %
%                                                                       %
% --------------------------------------------------------------------- %
%                                                                       %
% Created on Aug 07 19:37:41 2021                                       %
% @author: Paolo Castillo                                               %
% https://gitlab.com/paolo.castillo.eu/min-proyect                      %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

NOMBRE_ARCHIVO = "ley-de-presupuestos-inicial-y-vigentenivel-partidaa--mayo-2021.csv"

## Partidas debe ser un conjunto con pares id_partida y nombre_partida, los pares son únicos. 
partidas = set()

## Datos partidas debe ser un diccionario indexado por el ID de cada partida, y que contenga los datos
## de todos los subtítulos de esa partida. Es decir, la key es id_partida y el value los subtítulos de esa partida.
datos_partidas = dict()

monedas = {
    'EUR': 913.33,
    'USD': 819.33,
    'GBP': 1054.33,
    'PEN': 239.33,
    'BOB': 118.33,
}

## Esta función carga los datos desde el archivo. Debe completarla las partes que se indican.
def cargar_datos(archivo, partidas, datos_partidas):

    ####### ESTA PARTE DE ABAJO NO SE MODIFICA ######
    with open(archivo, "r", encoding="utf-8") as lineas:
        next(lineas)
        for linea in lineas:
            data = linea.strip("\n").split(";")[0].split(",")
            id = data[0]
            año = data[1]
            mes = data[2]
            moneda = "USD" if data[3]=='DOLARES' else "CLP"
            id_partida = data[4]
            if len(data) == 11:
                nombre_partida = data[5][1:] + data[6][:-1]
                id_subtitulo = data[7]
                nombre_subtitulo = data[8]
                monto_original = float(data[9].replace(".",""))
                monto_a_marzo = float(data[10].replace(".",""))
            elif len(data) == 12:
                nombre_partida = data[5][1:] + data[6] + data[7][:-1]
                id_subtitulo = data[8]
                nombre_subtitulo = data[9]
                monto_original = float(data[10].replace(".",""))
                monto_a_marzo = float(data[11].replace(".",""))
            else:
                nombre_partida = data[5]
                id_subtitulo = data[6]
                nombre_subtitulo = data[7]
                monto_original = float(data[8].replace(".",""))
                monto_a_marzo = float(data[9].replace(".",""))
            ####### ESTA PARTE DE ARRIBA NO SE MODIFICA ######

            ## Por cada línea se entrega la siguiente información: 
            ## id, año, mes, moneda, id_partida, nombre_partida, id_subtitulo, nombre_subtitulo, monto_original y monto_a_marzo

            ## 1. Agregar datos a estructuras apropiadas.
            ##    Construir conjunto con nombres de partidas
            ##    Partidas debe ser un conjunto con pares id_partida y nombre_partida. 
            ## COMPLETAR AQUí
            
            tupla = (id_partida, nombre_partida)
            partidas.add(tupla)
            
            ## 1. Agregar datos a estructuras apropiadas.
            ##    Construir diccionario con informacion de todas las ejecuciones de una partida
            ##    datos_partidas debe ser un diccionario indexado por el ID de cada partida, y que contenga los datos
            ##    de todos los subtítulos de esa partida.
            ## COMPLETAR AQUI

            # id_partida in datos_partidas.keys()
            if (verificar_detalle(id_partida, datos_partidas)):
                # datos_partidas[id_partida].append([id_subtitulo, nombre_subtitulo, monto_original, monto_a_marzo, año, mes, moneda])
                datos_partidas[id_partida].append((id_subtitulo, nombre_subtitulo, monto_original, monto_a_marzo, año, mes, moneda))
            else:
                datos_partidas[id_partida] = []
                # datos_partidas[id_partida].append([id_subtitulo, nombre_subtitulo, monto_original, monto_a_marzo, año, mes, moneda])
                datos_partidas[id_partida].append((id_subtitulo, nombre_subtitulo, monto_original, monto_a_marzo, año, mes, moneda))
                
            ##
            ##
            ##

    print("Datos leidos.")
    print("Cantidad de partidas distintas leidas:", len(partidas))
    print("")

def verificar_detalle(id_partida, detalle):
    return True if id_partida in detalle else False

## 2. Construir función que muestra el detalle de todos los subtítulos de una partida en particular.
##    Debe mostrar un menu con todas las partidas posibles y permitir al usuario elegir una.
##    Recibe como parámetros el conjunto de partidas existentes,
##    un diccionario con los datos de los subtítulos de cada partida y un parámetro de conversión de dólares a pesos.
##    
def montos_por_partida(partidas, datos_partidas, mensaje = ''):
    mostrar_partidas(partidas)
    print(mensaje)
    _partida = input(f"Ingrese un id de partida que quiera elegir : ")
    if (encontrar_partida(_partida, partidas)):
        mostrar_detalle(datos_partidas[_partida])
    else:
        montos_por_partida(partidas, datos_partidas, 'Error, partida no encontrada. Por favor repetir operación.')
        
def mostrar_partidas(partidas):
    for partida in partidas:
        print(f"Cod: [{partida[0]}] - Nombre: {partida[1]}")

def mostrar_detalle(datos_partidas):
    for detalle in datos_partidas:
        monto_original = convertir_a_pesos_cl(detalle[2], detalle[-1])
        monto_a_marzo = convertir_a_pesos_cl(detalle[3], detalle[-1])
        diferencia = convertir_a_pesos_cl(detalle[3] - detalle[2], detalle[-1])
        print(f"- {detalle[1]}, Monto Original {monto_original:.2f}, Monto a Marzo {monto_a_marzo:.2f},  Diferencia {diferencia:.2f}")
    
def encontrar_partida(id, partidas):
    return any(id in d for d in partidas)

def convertir_a_pesos_cl(monto, tipo='USD'):
    if tipo in monedas:
        return monto * monedas[tipo]
    return monto

## 3. Construir función que imprima datos de los montos totales originales de cada partida, ordenados de mayor a menor.
##    Recibe como parámetros el conjunto de partidas existentes,
##    un diccionario con los datos de los subtítulos de cada partida y un parámetro de conversión de dólares a pesos.
def partidas_por_monto_original(partidas, datos_partidas, usd_a_clp):
    _partidas = []
    for partida in partidas:
        monto_total = 0
        for detalle in datos_partidas[partida[0]]:
            monto_total += convertir_a_pesos_cl(detalle[2], detalle[-1])
        _partidas.append([partida, monto_total])
    mostrar_monto_original(_partidas)
    
def ordernar_partida_monto (partidas_monto):
    return sorted(partidas_monto, key = lambda x: x[1], reverse=True)

def mostrar_monto_original(partidas_monto,  text = 'Monto Total'):
    partidas_ordena = ordernar_partida_monto(partidas_monto)
    for partida in partidas_ordena:
        print(f"Cod: [{partida[0][0]}] - Nombre: {partida[0][1]}, {text} : {partida[1]:.2f}")
    
## 4. Agregar una función más que responda a una consulta definida por usted, usando los datos disponible en el archivo
##    Puede cambiar el nombre a la función, pero también deberá cambiarla en la opción 3 del menú.
##    Promedio
def funcion_personalizada(partidas, datos_partidas, usd_a_clp):
    _partidas = []
    for partida in partidas:
        monto_total = 0
        for detalle in datos_partidas[partida[0]]:
            monto_total += convertir_a_pesos_cl(detalle[2], detalle[-1])
        _partidas.append([partida, monto_total / len(datos_partidas[partida[0]])])
    mostrar_monto_original(_partidas, 'Promedio')

############################################################################
## No necesita modificar este código ##
if __name__ == "__main__":

    cargar_datos(NOMBRE_ARCHIVO, partidas, datos_partidas)

    dict_opciones = {1: ("Montos de una partida", montos_por_partida),
                     2: ("Partidas ordenadas de mayor a menor, por monto original", partidas_por_monto_original),
                     3: ("Función personalizada", funcion_personalizada), ## puede cambiar este nombre en la parte 4.
                     0: ("Salir", None)
                    }

    op = -1
    while op != 0:
        
        for k, v in dict_opciones.items():
            print(f"{k}: {v[0]}")
        
        try:
            op = int(input("Opcion: "))
        
        except ValueError:
            print(f"Ingrese opción valida")
            op = -1
        
        if op != 0 and op in dict_opciones.keys():
            dict_opciones[op][1](partidas, datos_partidas, 800)
